using Egads
import Esmorga as esmorga
using PyPlot, PyCall, LinearAlgebra, Printf
pygui(true)

cadfile   = joinpath(@__DIR__,"../DATA/carm.egads")
pX        = 3
pS        = 2*pX
egadsTess = false
nE        = 10

############### INFO LS #########
lsZ = esmorga.NLS_pars()
context, model, curves = esmorga.EGADS_curve(cadfile, egadsTess=egadsTess)
tc    = length(curves)


Q  = 20
nQ  = Q * nE
zw  = esmorga.gaussZW(esmorga.eGL,Q)
disp = zeros(2, tc)
dataX = zeros(2,tc, 3, nQ) ;
dataA = zeros(2,tc, 3, nQ) ;

for j = 1:tc
    nE0   = egadsTess ? -1 : nE
	xF,sF = esmorga.create_mesh(curves[j], nE0, pX,pS; fixX = true, fixS = true)
	disp[1,j] = sqrt(esmorga.get_disparity(xF,sF,curves[j]))
	sX,aS = esmorga.sample_curves(xF,sF, curves[j]; z = zw.nodes, make3d=true)
	dataX[1,j,:,:] = sX ; dataA[1,j,:,:] = aS ;
	info  = esmorga.mesh_optimization(lsZ, xF,sF, curves[j])#; printIO = true)
	sX,aS = esmorga.sample_curves(xF,sF, curves[j]; z = zw.nodes, make3d=true)
	dataX[2,j,:,:] = sX ; dataA[2,j,:,:] = aS ;
	disp[2,j] = sqrt(esmorga.get_disparity(xF,sF,curves[j]))
end


fs1   = 14
margs = ["right","top"]
fig  = figure(1)
ax = [fig.add_subplot(1, 2, 1, projection="3d"),fig.add_subplot(1, 2,2)]
par = esmorga.parula_palette()

piv = sortperm(disp[1,:])
for a = 1 : 2
	tit = a == 1 ? "curves" : "disparity"
	ax[a].set_title(tit)
	for j = 1:tc
		if a == 1
			ax[1].plot(dataX[1,j,1,:], dataX[1,j,2,:], dataX[1,j,3,:], color = par[1],zorder=0, ls = "-",lw = 2)
			ax[1].plot(dataA[1,j,1,:], dataA[1,j,2,:], dataA[1,j,3,:], color = par[5],zorder=0, ls = "--",lw = 2)
		else
			ax[2].plot([1:tc;], disp[1,piv], c= par[1], ls = "-")
			ax[2].plot([1:tc;], disp[2,piv], c = par[5], ls = "--")
		end
	end
	if a == 2
		ylims   = ax[a].get_ylim()
		ytk     = ax[a].get_yticks()
		ytk_exp = [@sprintf("%1.1e", ytk[j]) for j = 1:length(ytk)]
		ax[a].set_yticklabels(ytk_exp,color="k",fontsize=fs1)
		ax[a].set_yscale("log")#ticks([])
		[ax[a].spines[i].set_visible(false) for i in margs]
	else
		ax[a].grid(false)
		ax[a].axis("off")
	end
end
