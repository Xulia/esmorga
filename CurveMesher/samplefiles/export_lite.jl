using Egads

function export_model_stream(CADtype)
	CADfile = joinpath(@__DIR__,"DATA/"*CADtype*".stp")
	if !isfile(CADfile)
		CADfile = joinpath(@__DIR__,"DATA/"*CADtype*".egads")
	end

	if !isfile(CADfile)
		throw(@error(" CAD FILE $CADfile NOT FOUND !! "))
	end
    context = egads.Ccontext()
    model = egads.loadModel(context, 0, CADfile)
    str_exp = egads.exportModel(model)
    fn = joinpath(@__DIR__,splitext(basename(CADfile))[1])
    open(fn*".lite", "w") do io
            write(io,str_exp)
    end
end

for cad in ["cylinder"]# ["nacelle","pitts","plane"]
    export_model_stream(cad)
end
