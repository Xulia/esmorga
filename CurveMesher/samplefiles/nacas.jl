using Egads
import Esmorga as esmorga

using PyPlot, PyCall, LinearAlgebra, Printf

cadfile   = normpath(@__DIR__,"../DATA/naca5412.egads")
pX        = 1
pS        = 1
egadsTess = false
nE        = 5
############### INFO LS #########
infoLS = esmorga.NLS_pars()
context, model, curves = esmorga.EGADS_curve(cadfile, egadsTess=egadsTess)
tc    = length(curves)

dataXF = [] ; dataAF = [] ;dataSF = []
dataXM = [] ; dataAM = [] ;dataSM = []
dataXI = [] ; dataAI = [] ;dataSI = []

nQ  = 20
zw2 = esmorga.gaussZW(esmorga.eUNIF,2)
zw  = esmorga.gaussZW(esmorga.eGL,nQ)
pX2 = 2 ; pS2 = 3 * pX2


for j = 1:tc-1
    nE0   = egadsTess ? -1 : nE
	x1,s1 = esmorga.create_mesh(curves[j], nE0, 1,1; fixX = false, fixS = -1)
	info  = esmorga.mesh_optimization(infoLS, x1,s1, curves[j]; printIO = true)
	xF,sF = esmorga.p_refine_mesh(curves[j],pX2,pS2,x1,s1, true, true,esmorga.eGLL)

	#xM,sM = create_mesh(curves[j], nE0, pX2,pS2; fixX = false, fixS = -1)
	#info  = mesh_optimization(infoLS, xF,sF, curves[j])#; printIO = true)
	#info  = mesh_optimization(infoLS, xM,sM, curves[j])#; printIO = true)

	sX,aS = esmorga.sample_curves(xF,sF, curves[j]; z = zw.nodes)
	push!(dataXF, sX) ; push!(dataAF, aS)
	#sX,aS = sample_curves(xM,sM, curves[j]; z = zw.nodes)
	#push!(dataXM, sX) ; push!(dataAM, aS)

	xI,sI = esmorga.create_mesh(curves[j], nE0, pX2,pS2)

	sX,aS = esmorga.sample_curves(xI,sI, curves[j]; z = zw.nodes)
	push!(dataXI, sX) ; push!(dataAI, aS)

	s = esmorga.evaluate_object(sI, zw2.nodes)
	push!(dataSI, s) ;
	s = esmorga.evaluate_object(sF, zw2.nodes)
	push!(dataSF, s)
end



function plot_errors(n, x, a, nE, name ; x0 = nothing , a0 = nothing, x00 = nothing , a00 = nothing )
	fs1   = 14
	margs = ["right","top"]
	fig   = figure(1)
	ax    = fig.add_subplot(111)
	ax.set_xticks([])
	ax.set_yscale("log")#ticks([])
	[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
	par = esmorga.parula_palette()
	nQ  = length(a[1][1,:])
	z0  = [[0:1.0 / (nQ-1):1;] .+ (j-1) for j = 1:nQ]
	for j = 1:n
		z   = z0[j]
		err = [norm(x[j][:,l] .- a[j][:,l]) for l = 1:nQ]
		@info " SIZE ERR ", size(err)
		ax.plot(z,err,color = par[1],zorder=0, lw = 2)
		if x0 != nothing
			err = [norm(x0[j][:,l] .- a0[j][:,l]) for l = 1:nQ]
			ax.plot(z,err,color = par[3],zorder=0, lw = 1.5, ls = "--")
		end
		if x00 != nothing
			err = [norm(x00[j][:,l] .- a00[j][:,l]) for l = 1:nQ]
			ax.plot(z,err,color = par[5],zorder=0, lw = 1, ls = "-.")
		end
	end
	ylims = ax.get_ylim()
	for k = 0:nE * n
		dt = k ./ nE
		ax.plot( repeat([dt],2), ylims, ls = ":", color = par[7], lw = 0.5)
	end
	ytk = ax.get_yticks()
	ytk_exp = [@sprintf("%1.1e", ytk[j]) for j = 1:length(ytk)]
	ax.set_yticklabels(ytk_exp,color="k",fontsize=fs1)

	savefig(joinpath(@__DIR__,"RESULTS/"*name*".pdf"), bbox_inches="tight")
	@info " Created figure $name.pdf"
	close(fig)
end

plot_errors(2, dataXF, dataXI, nE,  "naca_errors" ; x0 = dataXI, a0 = dataXI)


function plot_curves(n, x, a, nE, name)
	fs1   = 14
	margs = ["right","top","left","bottom"]
	fig   = figure(1, figsize=(4,4))
	ax    = fig.add_subplot(111)
	ax.set_xticks([])
	ax.set_yticks([])
	[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
	par = esmorga.parula_palette()
	nQ  = length(a[1][1,:])
	z0  = [[0:1.0 / (nQ-1):1;] .+ (j-1) for j = 1:nQ]
	piv = [1:Int(nQ / nE): nQ;]
	xLIM = zeros(2)
	yLIM = zeros(2)
	for j = 1:n
		ax.plot(x[j][1,:],x[j][2,:], color = par[5],zorder=0, lw = 1)
		ax.plot(a[j][1,:],a[j][2,:], color = par[1],zorder=0, lw = 1, ls = "--")
		ax.scatter(x[j][1,piv],x[j][2,piv], color = par[1],zorder=0, s = 15)
		ax.scatter(a[j][1,piv],a[j][2,piv], color = par[1],zorder=0, s = 15)
		ax.scatter(x[j][1,piv],x[j][2,piv], color = par[5],zorder=1, s = 5)
		ax.scatter(a[j][1,piv],a[j][2,piv], color = par[5],zorder=1, s = 5)
		xLIM[1] = min(xLIM[1], minimum(a[1])) -0.1
		xLIM[2] = max(xLIM[2], maximum(a[1])) +0.1
		yLIM[1] = min(yLIM[1], minimum(a[2])) -0.1
		yLIM[2] = max(yLIM[2], maximum(a[2])) +0.1
	end
	ax.set_ylim(xLIM)
	ax.set_xlim(xLIM)
	savefig(joinpath(@__DIR__,"RESULTS/"*name*".pdf"), bbox_inches="tight")
	@info " Created figure $name.pdf"
	close(fig)
end

plot_curves(2, dataXI, dataAI, nE,  "naca_interpol")

plot_curves(2, dataXF, dataAF, nE,  "naca_fixed")
