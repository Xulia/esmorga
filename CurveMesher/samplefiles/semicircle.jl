


using Esmorga

using PyCall, PyPlot, LinearAlgebra, Printf
pygui(true)
pX     = 2 ;pS     = 10

curve    = Esmorga.auto_curve(Esmorga.eSEMICIRCLE)
typemesh = Esmorga.eEQUIPARAM
plotRefs = false


disp = zeros(3,3)
global i = 1
eles= [1,2,4]
for nE in eles
	par    = Esmorga.parula_palette()
	Q      = 150
	zw     = Esmorga.gaussZW(Esmorga.eUNIF, Q)

	mXI, mSI = Esmorga.create_mesh(curve, nE, pX, pS;fixX = false, fixS = -1, meshType=typemesh)
	mXF, mSF = Esmorga.create_mesh(curve, nE, pX, pS;fixX = true,  fixS = true, meshType=typemesh)
	mXM, mSM = Esmorga.create_mesh(curve, nE, pX, pS;fixX = false, fixS = -1, meshType=typemesh)

	infoLS = Esmorga.NLS_pars(;vars = Esmorga.eMoveXS,gTOL = 1.e-14)
	info   = Esmorga.mesh_optimization(infoLS, mXF,mSF, curve)
	info   = Esmorga.mesh_optimization(infoLS, mXM,mSM, curve)

	xi,ai = Esmorga.sample_curves(mXI, mSI, curve; z = zw.nodes, make3d = false)
	xf,af = Esmorga.sample_curves(mXF, mSF, curve; z = zw.nodes, make3d = false)
	xm,am = Esmorga.sample_curves(mXM, mSM, curve; z = zw.nodes, make3d = false)

	disp[1,i] = Esmorga.get_disparity(mXI,mSI,curve)
	disp[2,i] = Esmorga.get_disparity(mXF,mSF,curve)
	disp[3,i] = Esmorga.get_disparity(mXM,mSM,curve)
	global i = i + 1

	if plotRefs
		fig   = figure(1, figsize=(2,1))
		ax    = fig.gca()

		ax.axis("off")
		#ax.set_aspect("equal")
		ax.set_xticks([])
		piv = vcat(1,[Q:Q:size(xi,2);])

		l_width = 2 ; p_size = 30
		ax.plot(af[1,:], af[2,:], color = par[6],zorder=2, lw = l_width,ls = "-.")
		ax.scatter(af[1,piv], af[2,piv], color = par[6],zorder=1, s = 25)

		ax.plot(xf[1,:], xf[2,:], color = par[1],zorder=0, lw = l_width,ls = "-")
		ax.scatter(xf[1,piv], xf[2,piv], color = par[1],zorder=2, s = 20)

		ax.plot(xm[1,:], xm[2,:], color = par[4],zorder=0, lw = l_width,ls = "--")
		ax.scatter(xm[1,piv], xm[2,piv], color = par[4],zorder=3, s =10)
		savefig("Semi-Circle_P_"*string(pX)*"_N_"*string(nE)*".pdf")
		close(fig)
	end

	fig   = figure(2, frameon=false, figsize=(6.5,3))
	ax    = fig.gca()
    	margs = ["right","top"]
    	[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
	plt.rc("font",family = "serif",  size=19)
	plt.rc( "text", usetex=true)

	ax = fig.gca()
	np   = size(xi,2)
	lcol = [par[6], par[1], par[4]]
	lsty = ["-.","-","--"]
	xInt = vcat(1,[Q:Q:np;])
	plist = [1:np;]
	em = 1000.0 ;eM = 0.0
	for tp = 1 : 3
		s    = tp == 1 ? Esmorga.evaluate_object(mSI,zw.nodes,order=0) : tp == 2 ?
				 Esmorga.evaluate_object(mSF,zw.nodes,order=0) : Esmorga.evaluate_object(mSM,zw.nodes,order=0)

		atan = Esmorga.evaluate_curve(curve, s[1,:], 1)
		vI   = tp == 1 ? hcat([ xi[:,k] .- ai[:,k] for k = 1:np]...) : tp == 2 ?
			         hcat([ xf[:,k] .- af[:,k] for k = 1:np]...) : hcat([ xm[:,k] .- am[:,k] for k = 1:np]...)

		nV   = [norm(vI[:,k]) for k = 1:np]
		nT   = [norm(atan[2,:,k]) for k = 1:np]
		sI   = [ norm(nV[k]) < 1.e-14 ? 0.0 : -sign(vI[1,k] * atan[2,2,k] / (nV[k] * nT[k]) - vI[2,k] * atan[2,1,k]/(nV[k] * nT[k]))  for k = 1:np]

		ax.plot(plist, nV .* sI , color = lcol[tp], ls = lsty[tp], lw = 2)
		ax.scatter(plist[xInt], nV[xInt].* sI[xInt], s = 40 , color = lcol[tp], zorder=3)
		ax.scatter(plist[xInt], nV[xInt].* sI[xInt], s = 15 , color = "white", zorder=3)
		ax.plot(plist[[1,np]], zeros(2), color = "k", ls = ":", zorder = 0)
		em = min(em, minimum(nV .* sI))
		eM = max(eM, maximum(nV .* sI))
	end
	@info " ERRORS $em $eM"
	ax.set_xticks([])
	yt = [@sprintf("%1.1e",k) for k in [em,0.0,eM]]
	ax.set_yticks([em,0.0,eM])
	ax.set_yticklabels([yt[1], 0.0,yt[end]])
	name = joinpath(@__DIR__,"OPT_Semi-Circle_P_"*string(pX)*"_N_"*string(nE)*".pdf")
	savefig(name)
	close(fig)
end
par = Esmorga.parula_palette()
lcol = [par[6], par[1], par[4]]
lsty = ["-.","-","--"]
fig   = figure(2, frameon=false, figsize=(7,3.5))
ax    = fig.gca()
margs = ["right","top"]
[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
plt.rc("font",family = "serif",  size=22)
plt.rc( "text", usetex=true)
ax = fig.gca()
for j = 1 : 3
	ax.plot(eles, disp[j,:], ls = lsty[j], c = lcol[j], label=" ")
	ax.scatter(eles, disp[j,:], marker="o", s= 40, color = lcol[j])
	ax.scatter(eles, disp[j,:], marker="o", s= 5, color = "white",zorder=3)
end
ax.set_yscale("log")
ax.set_xscale("log")
ax.set_xticks(eles)
ax.set_xticklabels(["1","2","4"])
ax.xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter()) #Clear major tick labels
ax.tick_params(axis="x", which="minor", bottom=false)
#ax.legend(frameon=false, loc="upper right")

name = joinpath(@__DIR__,"OPT_Semi-Circle_P_"*string(pX)*"_rates.pdf")
savefig(name)
close(fig)
