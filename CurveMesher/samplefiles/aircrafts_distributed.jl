using Distributed

@everywhere begin
	using Distributed
	using DistributedArrays
	using EgadsLite
	using Esmorga


	function curves_hpc(cad::String, pX::Int, nE::Int)
		lsZ     = Esmorga.NLS_pars(;style=Esmorga.eZHANG, gTOL = 1.e-13, it=250)
		pS      = 3 * pX
		CADfile = joinpath(@__DIR__,"../DATA/"*cad*".lite")
		try isfile(CADfile)
		catch
			throw(@error " parallel stuff only allowed with lite files !")
		end
		aux = Esmorga.EGADS_curve(CADfile)
		curves, cid = aux.curves, aux.cids
		cid = [1:length(curves);]
		nc       = length(cid)
		nw       = min(nworkers(), nc)
		wl       = [1:nw;]
		ids      = distribute(cid, procs = wl)
		ctime = distribute(zeros(nc), procs = wl)
		disp0  = zeros(nc) ; disp1  = zeros(nc) ;
		iters = distribute(zeros(Int,nc), procs = wl)
		meshX = distribute(zeros(nc,3,nE*pX + 1), dist=(nw, 1,1), procs = wl)
		meshS = distribute(zeros(nc,1,nE*pS + 1), dist=(nw, 1,1), procs = wl)


		timeOpt = @elapsed @sync begin
			for j = 1 : nw
				@spawnat j Esmorga.partitioned_mesh_optimization(CADfile,lsZ,pX,pS,nE,localpart(ids), localpart(ctime), localpart(iters),localpart(meshX),localpart(meshS))
			end
		end

		 timeOpt = @elapsed @sync begin
                        for j = 1 : nw
                                @spawnat j Esmorga.partitioned_mesh_optimization(CADfile,lsZ,pX,pS,nE,localpart(ids), localpart(ctime), localpart(iters),localpart(meshX),localpart(meshS))
                        end
                end

		 timeOpt = @elapsed @sync begin
                        for j = 1 : nw
                                @spawnat j Esmorga.partitioned_mesh_optimization(CADfile,lsZ,pX,pS,nE,localpart(ids), localpart(ctime), localpart(iters),localpart(meshX),localpart(meshS))
                        end
                end



		for j = 1 : nc
			C = curves[cid[j]]
			xI,sI = Esmorga.create_mesh(C, nE, pX,pS);
			disp0[j] = sqrt(Esmorga.get_disparity(xI, sI,C))
			xI.nodes = meshX[j,C.plane,:]
			sI.nodes = meshS[j,:,:]
			disp1[j] = sqrt(Esmorga.get_disparity(xI, sI,C))
		end

		fname = joinpath(@__DIR__,"RESULTS/"*cad*"_pol_"*string(pX)*"_ele_"*string(nE)*"_procs_"*string(nw)*"_curves.dat")
		@info " CURVES: Optimization time   $cad ---> $pX  $nE for $timeOpt  $nc curves and $(nworkers()) CPUs"
		open(fname, "w") do io
			println(io,cad," ",pX," ",nE," ",nc," ",nw," ",timeOpt)
			for j = 1 : nc
				println(io,disp0[j]," ",disp1[j]," ",iters[j]," ",ctime[j])
			end
		end
	end

	function part_element_optimization(CADfile::String,curveID::Int,lsZ::Esmorga.NLS_pars, pX::Int,pS::Int,nE::Int,
		eles::Vector, iters::Vector, nx::Array{Float64, 3},ns:: Array{Float64, 3})
		curve   = EGADS_curve(CADfile, curveID = curveID)[3]
		mxE,msE = Esmorga.create_partitioned_mesh(curve,pX,pS,nE)
		px      = [1:pX+1;] ; ps = [1:pS+1;]
		ctt     = filter(x -> !(x in curve.plane), [1,2,3])
		t0      = time()
		for k = 1:length(eles)
			j = eles[k]
			_,_,iters[k],_,_ =  Esmorga.mesh_optimization(lsZ,mxE[j],msE[j],curve)
			ns[k,:,:] = msE[j].nodes
			if curve.d != 3
				nx[k,curve.plane,:] .= mxE[j].nodes
				for d = 1:length(ctt)
					nx[k,ctt[d],:] .= curve.cttPlane[d]
				end
			else
				nx[k,:,:] .= mxE[j].nodes
			end
		end
	end

	function elements_hpc(cad::String, pX::Int, nE::Int)
		lsZ     = Esmorga.NLS_pars(;style=Esmorga.eZHANG, gTOL = 1.e-13)
		pS      = 3 * pX

		CADfile = joinpath(@__DIR__,"DATA/"*cad*".lite")
		try isfile(CADfile)
		catch
			throw(@error " parallel stuff only allowed with lite files !")
		end

		aux = EGADS_curve(CADfile)
		curves,cids = aux.curves ,aux.cids
		nc     = length(curves)
		nw     = min(nworkers(), nE)
		wl     = [1:nw;]
		ids    = distribute([1:nE;], procs = wl)
		cit    = distribute(zeros(Int,nE), procs = wl)

		ctime  = zeros(nc)
		disp0   = zeros(nc) ; disp1   = zeros(nc) ;
		iters  = zeros(Int,nc)

		meshX  = distribute(zeros(nE,3,pX + 1), dist=(nw,1,1), procs = wl)
		meshS  = distribute(zeros(nE,1,pS + 1), dist=(nw,1,1), procs = wl)
		timeOpt = @elapsed for c = 1 : nc
			ctime[c] = @elapsed @sync begin
				for j = 1 : nw
					@spawnat j part_element_optimization(CADfile,cids[c],lsZ,pX,pS,nE,localpart(ids),localpart(cit),localpart(meshX),localpart(meshS))
				end
			end
			C = curves[c]
			xI,sI = create_mesh(C, nE, pX,pS, meshType=Esmorga.eEQUIPARAM);
			disp0[c] = sqrt(Esmorga.get_disparity(xI,sI,C))
			Esmorga.refill_nodes!(C,xI.nodes, pX, convert(Array,meshX))
			Esmorga.refill_nodes!(C,sI.nodes, pS, convert(Array,meshS))
			disp1[c] = sqrt(Esmorga.get_disparity(xI,sI,C))
			iters[c] = maximum(cit)
		end

		fname = joinpath(@__DIR__,"NACRES/"*cad*"_curve_pol_"*string(pX)*"_ele_"*string(nE)*"_procs_"*string(nw)*".dat")

		@info " ELEMENTS: Optimization time  $cad ---> $pX  $nE $timeOpt $nc curves and $(nworkers()) CPUs"
		open(fname, "w") do io
			println(io,cad," ",pX," ",nE," ",nc," ",nw," ",timeOpt)
			for j = 1 : nc
				println(io,disp0[j]," ",disp1[j]," ",iters[j]," ",ctime[j])
			end
		end
	end

end

tplane = @elapsed curves_hpc("plane",3,48)
tpitts = @elapsed curves_hpc("pitts",2,48)


@info " TIMES ELEMENTS PITTS p2 $tpitts PLANE p3  $tplane"
