using Esmorga, PyCall, PyPlot
using Printf

printIO= false
nE0    = 1
nR     = 5
p1     = 2
p2     = 4
pM     = p2 - p1 +1

infoS  = Esmorga.NLS_pars(;vars = Esmorga.eMoveS,gTOL = 1.e-14)
infoXS = Esmorga.NLS_pars(;vars = Esmorga.eMoveXS,gTOL = 1.e-14)

curve  = Esmorga.auto_curve(Esmorga.eCIRCLE)

zGL  = Esmorga.gaussZW(Esmorga.eGL ,20)
zGLL = Esmorga.gaussZW(Esmorga.eGLL,20)

global data = zeros(pM,2,nR + 1)
global ele  = [nE0*2^j for j = 0:nR]

zw = Esmorga.gaussZW(Esmorga.eGLL, 20)

for e = 1:nR+1
    for p = 1:pM
        pX = p1 + p -1
        pS = 3 * pX
        mX1, mS1    = Esmorga.create_mesh(curve,ele[e],pX,pS,fixX = false, fixS = -1)
        mX2, mS2    = Esmorga.create_mesh(curve,ele[e],pX,pS,fixX = false, fixS = -1)
        #info1       = mesh_optimization(infoS, mX1,mS1,curve)
        info2       = Esmorga.mesh_optimization(infoXS,mX2,mS2,curve)
        data[p,1,e] = sqrt(Esmorga.get_disparity(mX1,mS1,curve))
        data[p,2,e] = sqrt(Esmorga.get_disparity(mX2,mS2,curve))
    end
end

for p = 1:pM
    println(" POLYNOMIAL APPROX ",p1+p-1)
    for j = 1:nR
        rt = log10(ele[j+1] / ele[j])
        println(" DATA 1 ", data[p,1,j], " RATE 1 ", log10(data[p,1,j]/data[p,1,j+1]) / rt)
        println(" DATA 2 ", data[p,2,j], " RATE 2 ", log10(data[p,2,j]/data[p,2,j+1]) / rt)
    end
end


function convergence_plots(elmts, p1, p2, data, d, cname)

    ## CONVERGENCE PLOTS
    fig   = figure(2, frameon=false, figsize=(5.0,5.5))
    ax    = fig.gca()
    margs = ["right","top"]
    [ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
	plt.rc("font",family = "serif",  size=20)
	plt.rc( "text", usetex=true)


    ax.set_yscale("log") ;    ax.set_xscale("log")

    wi  = 3
    fs1 = 20
    fs2 = 25

    clr = Esmorga.parula_palette()

    pM = p2 - p1 + 1
	sd = size(data)
	mk = ["o","o","o"]
    for p = 1:pM

		ax.plot(elmts,data[p,1,:],  lw = 3, color = clr[7], "-.", zorder = 1)
		#ax.scatter(elmts,data[p,1,:], color ="w", marker = mk[p], s = 1, zorder = 4)

		ax.plot(elmts,data[p,2,:],  lw = 4, color = clr[2], ":" , zorder = 3)
		ax.scatter(elmts,data[p,2,:], color = clr[2], marker = mk[p], s = 40, zorder = 4)
		ax.scatter(elmts,data[p,2,:], color ="w", marker = mk[p], s = 1, zorder = 4)

		if sd[2] == 3
			ax.plot(elmts,data[p,3,:],  lw =2, color = clr[1], "-")
			ax.scatter(elmts,data[p,3,:], color = clr[1], marker = mk[p], s = 40, zorder = 4)
		end
    end

    ym = minimum(data[end,:,end]*0.8)
	yM = maximum(data[1,:,1]*1.2)
    ax.set_ylim(ym,yM)
    y_ticks    = fill(yM, 5)
    y_ticks[5] = ym

    y_ticks[3] = sqrt(y_ticks[1] * y_ticks[5])
    y_ticks[2] = sqrt(y_ticks[1] * y_ticks[3])
    y_ticks[4] = sqrt(y_ticks[3] * y_ticks[5])


    minorticks_off()
    ax.set_xticks(elmts)
    ylabs  = [ @sprintf("%1.1e", y_ticks[j]) for j = 1:length(y_ticks)]

    ax.set_xticklabels(elmts,color="k",fontsize=fs1)
    #ax.set_xlabel("Elements",size=fs2)

    ax.set_yticks(y_ticks)
    ax.set_yticklabels(ylabs,color="k",fontsize=fs1)
    savefig(joinpath(@__DIR__,"RESULTS/"*cname*"_rates.pdf"), bbox_inches="tight")
    close()
end


convergence_plots(ele,p1,p2, data,curve.d,"CIRCLE")
