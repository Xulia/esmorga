
import Egads as egads
import Esmorga
using LinearAlgebra,PyCall, PyPlot, Printf
pygui(true)

#CADtype = "cylinder"
CADtype = "nacelle"
CADfile = joinpath(@__DIR__,"DATA/"*CADtype*".stp")
if !isfile(CADfile)
	CADfile = joinpath(@__DIR__,"DATA/"*CADtype*".egads")
end

if !isfile(CADfile)
	throw(@error(" CAD FILE $CADfile NOT FOUND !! "))
end

pX = 2 ; pS = 3 * pX ; nE = 12
lsZ = Esmorga.NLS_pars(;style=Esmorga.eZHANG, gTOL = 1.e-12)
context, model, curves = Esmorga.EGADS_curve(CADfile)

mt    = Esmorga.eEQUIPARAM
dIM   = [Esmorga.initial_disparity(c,nE,pX,pS, meshType = mt, arcNorm = true) for c in curves]
piv   = findall(dIM .> 1.e-12) ;
n     = length(piv)
iters = zeros(Int,2,n)
lsear = zeros(Int,2,n)
grads = zeros(2,n)
disps = zeros(2,n)
time  = zeros(2,n)

nxF = zeros(n, 3, pX* nE+1)
nsF = zeros(n, 1, pS* nE+1)

nxM = zeros(n, 3, pX* nE+1)
nsM = zeros(n, 1, pS* nE+1)

nxI = zeros(n, 3, pX* nE+1)
nsI = zeros(n, 1, pS* nE+1)

for j = 1:n

	C = curves[piv[j]]
	xF,sF = Esmorga.create_mesh(C, nE, pX,pS; fixX = true, fixS = true, meshType = mt)
	nxI[j,:,:] .= Esmorga.get_3d_nodes(xF, C) ; nsI[j,:,:] .= sF.nodes ;

	k = 1
	time[1,j] = @elapsed aux = Esmorga.mesh_optimization(lsZ, xF,sF, C)
	nxF[j,:,:] .= Esmorga.get_3d_nodes(xF, C)  ; nsF[j,:,:] .= sF.nodes ;
	disps[k,j] = aux[1] ; iters[k,j] = aux[3] ; lsear[k,j] = aux[4] ; grads[k,j] = aux[6]

	k = 2
	xM,sM = Esmorga.create_mesh(C, nE, pX,pS; fixX = false, fixS = -1, meshType = mt)
	time[2,j] = @elapsed aux = Esmorga.mesh_optimization(lsZ, xM,sM, C)
	nxM[j,:,:] = Esmorga.get_3d_nodes(xM, C)  ; nsM[j,:,:] = sM.nodes ;
	disps[k,j] = aux[1] ; iters[k,j] = aux[3] ; lsear[k,j] = aux[4] ; grads[k,j] = aux[6]
end

Q    = 15 ; zw = Esmorga.gaussZW(Esmorga.eGL, Q)

n = length(piv)
fnames = ["initial","constr","unconst"]

eMAX = 0.0004 ; eMIN = 1.e-12
for t = 4 : 4
	fig = t != 4 ? plt.figure(figsize=(10, 10), frameon=false) :
				   plt.figure(figsize=(7,5), frameon=false)
	ax  = fig.add_subplot(111,projection="3d")
	plt.rc("font",family = "sans-serif",  size=18)
	plt.rc( "text", usetex=true)
	ax.grid(false)
	ax.axis("off")
	paleta = nothing

	for j = 1 : n
		C = curves[piv[j]]
		C.d = 3 ; C.plane = [1,2,3]
		xF,sF = Esmorga.create_mesh(C, nE, pX,pS; fixX = true, fixS = true, meshType = mt)
		xF.nodes = t== 1 ? nxI[j,:,:] : t == 2 ? nxF[j,:,:] : nxM[j,:,:]
		sF.nodes = t== 1 ? nsI[j,:,:] : t == 2 ? nsF[j,:,:] : nsM[j,:,:]
		x,a = Esmorga.sample_curves(xF, sF, C, z = zw.nodes, make3d = true)
		e = [norm(x[:,k] - a[:,k]) for k = 1 : nE * Q ]
		paleta = ax.scatter(a[1,:],a[2,:],a[3,:], c= e,  norm=matplotlib.colors.LogNorm(vmin=eMIN, vmax=eMAX), s = 20, marker=".")
	end
	clines = setdiff([1:length(dIM);],piv)
	for j = 1 : length(clines)
		C = curves[clines[j]]
		C.d = 3 ; C.plane = [1,2,3]
		xF,sF = Esmorga.create_mesh(C, nE, pX,pS; fixX = true, fixS = true, meshType = mt)
		x,a = Esmorga.sample_curves(xF, sF, C, z = zw.nodes, make3d = true)
		e = [norm(x[:,k] - a[:,k]) for k = 1 : nE * Q ]
		paleta = ax.scatter(a[1,:],a[2,:],a[3,:], c= e,  norm=matplotlib.colors.LogNorm(vmin=eMIN, vmax=eMAX),s = 20, marker=".")
	end
	if t == 4
		fig.gca().set_visible(false)
		cb = fig.colorbar(paleta, orientation="horizontal", aspect=40)#, cax=ax, shrink=0.5, aspect=5)
		cb.ax.minorticks_off()
		savefig("nacelle_pw_errors_"*string(pX)*"_eles_"*string(nE)*"_palette.pdf")
	else
		ax.view_init(elev=5, azim=100)
		savefig("nacelle_pw_errors_"*string(pX)*"_eles_"*string(nE)*"_"*fnames[t]*".pdf")
	end
	close(fig)
end



sqrt_disps= zeros(2,n)
for j = 1 : n
	C = curves[piv[j]]
	info  = egads.getRange(C.ego)
	arc   = egads.arcLength(C.ego,info.range[1], info.range[2])
	sqrt_disps[1,j] = sqrt(disps[1,j]) / arc
	sqrt_disps[2,j] = sqrt(disps[2,j]) / arc
end
pal = Esmorga.parula_palette()


fig = plt.figure(figsize=(6,4))
plt.rc("font",family = "sans-serif",  size=21)
plt.rc( "text", usetex=true)
ax  = fig.gca()
n = length(piv)
per = sortperm(dIM[piv])
ax.plot(dIM[piv[per]], ls = "-", c = pal[1], lw = 3)
ax.plot(sqrt_disps[1,per], ls = "--", c = pal[4], lw = 3)
ax.plot(sqrt_disps[2,per], ls = "-.", c = pal[7], lw = 3)

#ax.plot(grads[1,per], ls = "-.", c = pal[7], lw = 3)
yt = 10.0 .^ [-1,-4,-7,-13]
yl = [@sprintf("%1.1e", k) for k in yt]
ax.set_yscale("log")
ax.set_xticks(vcat(1,[7:7:n;]))
#ax.set_yticks(yt)
#ax.set_yticklabels(yl)

margs = ["right","top"]
[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
#ax.set_ylabel(ax.get_ylabel(),fontsize=24)
savefig("nacelle_disparities_"*string(pX)*".pdf")
close(fig)
