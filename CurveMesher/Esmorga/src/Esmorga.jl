

module Esmorga
	using FastGaussQuadrature, Jacobi, Polynomials, Printf, Einsum,LinearAlgebra, PyCall, PyPlot, Requires
	pygui(true)
	ego = nothing

	function __init__()

		@require Egads="5055658d-4dc9-4a0b-9da6-bd9a55049517" begin
			@info " Using Egads !! "
			ego   = Egads.Ego
			egads = Egads
		end

		@require EgadsLite="f90fc4c3-c8d2-4ed6-a8ee-a0dac9361e36" begin
			@info " Using EgadsLite !! "
			ego   = EgadsLite.Ego
			egads = EgadsLite
		end
	end

	const GQ = FastGaussQuadrature

	# TYPE DEFS
	const int  = Union{Int32,Int64}
	const dbl  = Float64
	const vDbl = Vector{Float64}
	const mDbl = Matrix{Float64}
	const mInt = Matrix{Int}
	const vInt = Union{Vector{Int32},Vector{Int64}}

	# Initial node distribution
	const eARCLENGTH  = "ArcLength"
	const eEQUIPARAM  = "Equi-param"

	#Type of nodes: initial interpolation points
	const eGLL        = "Gauss-Lobatto-Legendre"
	const eGL         = "Gauss-Legendre"
	const eUNIF       = "Uniform"

	#Analytic curves: test suite
	const eCIRCLE     = 1
	const eCIRCLEPOL  = 2
	const eSEMICIRCLE = 3
	const eCIRCLEXY   = 4
	const eCIRCLE3D   = 5
	const eCAD        = 6
	const eSPIRAL     = 7
	const eCUBIC      = 8
	const ePETALS     = 9
	const ePETALS3D   = 10
	const eHELIX      = 11
	const eARCHYTAS   = 12
	const eARCSPIRAL  = 13
	const eNACA       = 14
	const ePOLY3D     = 15
	const eSPIRAL3D   = 16
	const eSPIRAL3D2  = 17
	const eSPHERE     = 18
	const eLEMNISCATA = 19
	const ePOLY2D     = 20

	# Newton options
	const eMoveX      = "x  "
	const eMoveS      = "s  "
	const eMoveXS     = "x,s"
	const eNOLS       = "NONE"
	const eARMIJO     = "Armijo"
	const eZHANG      = "Zhang"
	const eL2         = "normL2"
	const eLinf       = "normInfty"
	const eDISP       = "disparity_l2"
	const eDISP_SIGMA = "disparity_dx"
	const eGEOMERR    = -100
	const eTRYPENALTY = 0
	const eSUCCESS    = 1
	const eMAXITERS   = 2
	const eGRADTOL    = 3
	const eSTEPTOL    = 4
	const eMAXLS      = 5
	const eFUNTOL     = 6

	"""
	structure with information for the optimizer:
	Members:
	--------

	fobj:
		objective function: disparity_sigma (default) or disparity_l2
	vars:
		moving variables: xs (default) or x, s
	norm:
		norm type used when computing gradient, step: l2 (default) or linf
	name:
		line search type: Zhang (default), Armijo or none
	it:
		maximum number of Newton iterations (default 500)
	gTOL:
		stop gradient criteria (default 1.e-12)
	sTOL:
		stop step criteria (default 1.e-12)
	fTOL:
		stop fobj criteria (default 1.e-12)
	ls:
		maximum number of line searches (default 20)
	rho:
		line search decrease parameter (deault 1/2)
	sigma,delta:
		Wolfe condition: delta=0.9, sigma=1.e-4
	q, c, f:
		parameters needed by optimizer
	eta:
		type of nonmonotone search (default average)
	f:
		previouse disparity value
	"""
	mutable struct NLS_pars
		fobj  :: String
		vars  :: String
		norm  :: String
		name  :: String
		it    :: Int64
		gTOL  :: Float64
		sTOL  :: Float64
		fTOL  :: Float64
		ls    :: Int64
		rho   :: Float64
		sigma :: Float64
		delta :: Float64
		q     :: Float64
		c     :: Float64
		eta   :: Float64
		f     :: Float64
		function NLS_pars(;f_obj = eDISP_SIGMA, vars= eMoveXS,ntype = eL2,style=eZHANG,it=500,gTOL = 1.e-13, sTOL= 1.e-14, fTOL=1.e-33,ls = 40, rho=0.5, sigma =0.9, delta=1.e-4)
			return new(f_obj, vars, ntype, style, it, gTOL, sTOL, fTOL, ls, rho, sigma, delta, 1.0, 0.0, 1.0, 0.0)
		end
	end

	include("geometry.jl")
	include("optimizer.jl")




	function parula_palette()
		rgb255=[(53,42,135)
			   (15,92,221)
			   (18,125,216)
			   (7,156,207)
			   (21,177,180)
			   (89,189,140)
			   (165,190,107)
			   (225,185,82)
			   (252,206,46)
			   (249,251,14)]
		return [rgb255[j]./255 for j = 1:10]
	end



	function curve_plot(aO, xO; aI = nothing, xI = nothing, file = nothing, open = true)
		margs = ["right","top","left","bottom"]
		dim   = length(aO[:,1])
		if open
			fig = figure(1)
			if dim == 3
				ax  = [fig.add_subplot(121,projection="3d"), fig.add_subplot(122)]
				ax[1].view_init(elev=10., azim=30)
				ax[1].set_facecolor("white")
				ax[1].w_xaxis.pane.fill = false
				ax[1].w_yaxis.pane.fill = false
				ax[1].w_zaxis.pane.fill = false
				ax[1].grid(false)
			else
				ax  = [fig.add_subplot(121), fig.add_subplot(122)]
			end
			ax[1].set_xticks([])
			ax[1].set_yticks([])
			[ax[1].spines[margs[i]].set_visible(false) for i in 1:length(margs)]
		else
			fig = gcf()
			ax = fig.get_axes()
		end

		c1 = "r"
		c2 = "b"
		lwi = 1
		if open
			lab1 = "x*"
			lab2 = "a o s*"
		else
			lab1 = "";lab2 = ""
		end
		if dim == 3
			ax[1].plot(aO[1,:], aO[2,:], aO[3,:], c= c1, ls= "-", lw = lwi, label = lab1)
			ax[1].plot(xO[1,:], xO[2,:], aO[3,:], c= c2, ls= "-", lw = lwi, label = lab2)
		else
			ax[1].plot(aO[1,:], aO[2,:], c= c1, ls= "-", lw = lwi,label = lab1)
			ax[1].plot(xO[1,:], xO[2,:], c= c2, ls= "-", lw = lwi,label = lab2)
		end
		if open
			lab1 = "|x - a o s|*"
			lab2 = "|x* - a o s*|"
		else
			lab1 = "";lab2 = ""
		end
		if aI != nothing
			eI = [norm(xI[:,j] .- aI[:,j]) for j = 1: length(xI[1,:])]
			ax[2].plot(eI, c= c1, ls= "--", lw = lwi,zorder=3, label = lab1)
		end
		eO = [norm(xO[:,j] .- aO[:,j]) for j = 1: length(xO[1,:])]
		ax[2].plot(eO, c= c2, ls= "-", lw = lwi, label = lab2)
		if file != nothing
			ax[1].legend(loc="upper center")
			ax[2].legend(loc="upper center")
			savefig(file*".pdf", bbox_inches="tight")
			close(fig)
		end
	end

	function plot_s(n, s, name)
		margs = ["right","top","left","bottom"]
		fig   = figure(1)
		ax    = fig.add_subplot(111)
		ax.set_xticks([])
		ax.set_yticks([])
		[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
		par = parula_palette()
		for j = 1:n
			ax.plot(s[j][1,:], zeros(length(s[j][1,:])), color = par[1],zorder=0, lw = 1)
			ax.scatter(s[j][1,:], zeros(length(s[j][1,:])), color = par[1], s=30, zorder=0)
			ax.scatter(s[j][1,:], zeros(length(s[j][1,:])), color = par[5], s=15,zorder=2)
		end
		savefig(name*".pdf", bbox_inches="tight")
		@info " Created figure $name.pdf"
		close(fig)
	end

	function plot_error(n, x, a, Q, name ; x0 = nothing , a0 = nothing)

		margs = ["right","top"]
		fig   = figure(1)
		ax    = fig.add_subplot(111)
		#ax.set_xticks([])
		#ax.set_yticks([])
		[ax.spines[margs[i]].set_visible(false) for i in 1:length(margs)]
		par = parula_palette(1)
		nQ  = length(a[1][1,:])
		piv = vcat(1,[Q:Q:nQ;])
		z0   = [0:1.0 / (nQ-1):1;]
		for j = 1:n
			z = 1 * (j-1) .+ z0
			err = [norm(x[j][:,l] .- a[j][:,l]) for l = 1:nQ]
			@info " SIZE ERR ", size(err)
			ax.plot(z,err,color = par[1],zorder=0, lw = 1)
			ax.scatter(z[piv],err[piv], color = par[1], s=30, zorder=0)
			if x0 != nothing
				err = [norm(x0[j][:,l] .- a0[j][:,l]) for l = 1:nQ]
				ax.plot(z,err,color = par[4],zorder=0, lw = 1)
				ax.scatter(z[piv],err[piv], color = par[4], s=30, zorder=0)
			end
		end
		savefig(name*".pdf", bbox_inches="tight")
		@info " Created figure $name.pdf"
		close(fig)
	end

end
