#=
 ******************************************************************************
 * ESMORGA:Exploiting Superconvergence in Meshes for Optimal Representations  *
 *                                        of the Geometry with high Accuracy  *
 *                                                                            *
 * This file is part of the esmorga module: optimization functions            *
 * Written by Xulia Docampo @ BSC-CNS                                         *
 *                                                                            *
 * Licensed under the GNU Lesser General Public License, version 2.1          *
 * See http://www.opensource.org/licenses/lgpl-2.1.php                        *
 ******************************************************************************
 =#


#= This is for internal use alone. Optimization using Newton:
only visible function:
  mesh_optimization(sInfo, meshX, meshS, curve, zw = nothing, printIO = false)
=#

function update_G(type, vars, meshX, meshS, curve,mu=0.0, zw = nothing)
    grad,h,grad_disp, grad_barrier = update_GH(type, vars, meshX, meshS, curve, mu,zw)
	xsID          = get_moving_index(meshX, meshS, vars)
    return grad[xsID],grad_disp[xsID],grad_barrier[xsID]
end

function update_H(type, vars, meshX, meshS, curve, mu=0.0, Q = -1)
    _,hess,_,_ = update_GH(type, vars, meshX, meshS, curve, z, w,mu,Q)
    return hess
end

function update_GH(type, vars, meshX, meshS, curve, mu = 0.0, zw = nothing)

    (zw == nothing) && (zw = gaussZW(eGLL, curve.d*2 * max(meshX.p, meshS.p)))

	meshX.phi = create_shape_funs(meshX.p, zw.nodes)
	meshS.phi = create_shape_funs(meshS.p, zw.nodes)

    lv  = ( meshX.p + 1 ) * meshX.d
    lw  = ( meshS.p + 1 ) * meshS.d

    v   = zeros(2,meshX.d, zw.Q, lv)
    w   = zeros(2,meshS.d, zw.Q, lw)

    for d in 1:meshX.d
        v[1,d,:,d:meshX.d:lv] = meshX.phi[1,:,:]
        v[2,d,:,d:meshX.d:lv] = meshX.phi[2,:,:]
    end

    for d in 1:meshS.d
        w[1,d,:,d:meshS.d:lw] = meshS.phi[1,:,:]
        w[2,d,:,d:meshS.d:lw] = meshS.phi[2,:,:]
    end

    # TOTAL X AND U NODES
    nX  = length(vec(meshX.nodes))
    nS  = length(vec(meshS.nodes))

    grad_obj     = zeros(nX + nS)
    grad_barrier = zeros(nX + nS)
    hess         = zeros(nX + nS, nX + nS) # FOR NOW, FULL SPARSE MATRIX
	for ele in 1:meshX.nE
        # QPoints corresponding to element
        xeID  =       meshX.eleColNodes[ele,:]
		seID  = nX .+ meshS.eleColNodes[ele,:]
        # GET MESH AND CURVE VALUES AT QPoints
        x = evaluate_object(meshX, zw.nodes,order=2,id=ele)
        s = evaluate_object(meshS, zw.nodes,order=2,id=ele)
        a = evaluate_object(curve, s[1,1,:],order=2,id=ele)

		xsID = vcat(xeID, seID)
		locG = local_G(type,x[1,:,:], x[2,:,:], a[1,:,:], a[2,:,:],
					    lv,lw, v[1,:,:,:], v[2,:,:,:],w[1,:,:,:],zw.weights)
		grad_obj[xsID]  .+= locG
		hxx = local_HXX(type, x[1,:,:], x[2,:,:], a[1,:,:],lv,v[1,:,:,:], v[2,:,:,:],zw.weights)
		hess[xeID,xeID] .+= hxx

		hxs = local_HXS(type, x[1,:,:], x[2,:,:], a[1,:,:], a[2,:,:],
						lv,lw, v[1,:,:,:],v[2,:,:,:],w[1,:,:,:],zw.weights)
		hess[xeID,seID] .+= hxs
		hess[seID,xeID] .+= hxs'


		hss = local_HSS(type,  x[1,:,:], x[2,:,:], a[1,:,:], a[2,:,:], a[3,:,:],
						 lw,w[1,:,:,:],zw.weights)
		hess[seID,seID] .+= hss
        # ADD BARRIER
		if abs(mu) > 1.e-33
			for r in 1:lw
				grad_barrier[seID[r]] += mu * sum(w[2,1,:,r] ./ (Float64(curve.dir) .*s[2,1,:]) .*zw.weights )
				for j in 1:lw
	                hess[seID[r],seID[j]] -= mu*sum(w[2,1,:,r] .* w[2,1,:,j] .* zw.weights ./ s[2,1,:].^2)
	            end
			end
		end
    end
    # SET = 0 FIXED NODES (INTERFACES; BOUNDARIES, ETC)
	grad_tot = grad_obj .+ grad_barrier
	return grad_tot, hess, grad_obj, grad_barrier
end

function local_G(type,  x, dx, a, da, lv, lw, vT, dvT, wT, wQ)
	Q = length(wQ)
    grad  = zeros(lv+lw)
    xa    = x .- a
    xada  = [dot(xa[:,j], da[:,j]) for j in 1:Q]
    if type == eDISP
        @einsum grad[r] = vT[d,k,r] * xa[d,k] * wQ[k]
        grad[lv+1:end] = -[sum(wT[1,:,r] .* xada .* wQ) for r in 1:lw]
    else
        ndx = [norm(dx[:,j]) for j in 1:Q]
        xa2 = [dot(xa[:,j],xa[:,j]) for j in 1:Q]
        @einsum grad[r] = (vT[d,k,r] * xa[d,k] * ndx[k] + 0.5 * dvT[d,k,r] * dx[d,k] / ndx[k] * xa2[k]) *wQ[k]
        grad[1+lv:end] = -[sum(xada .* wT[1,:,r].* wQ .* ndx) for r in 1:lw]
    end
    return grad
end

function local_HXX(type,  x, dx, a, lv, v, dv, wQ)
	Q = length(wQ)
    hess = zeros(lv,lv)
    xa   = x .- a
	if type == eDISP
        @einsum hess[r,j] = v[d,k,r] * v[d,k,j] * wQ[k]
    else
        ndx = [norm(dx[:,j]) for j in 1:Q]
        xa2 = [dot(xa[:,j],xa[:,j]) for j in 1:Q]
        for r in 1:lv
            vr     =  v[:,:,r]
            dvr    = dv[:,:,r]
            dx_dvr = [dot(dv[:,k,r],dx[:,k]) for k in 1:Q]
            d_dnx  = dx_dvr ./ ndx
            xa_vr  = [dot(v[:,k,r] , xa[:,k]) for k in 1:Q]
            for j in 1:lv
                aux = [dot(v[:,k,j],v[:,k,r].*ndx[k].+xa[:,k].*d_dnx[k]) for k in 1:Q]
                hess[r,j] = sum(aux.*wQ)
                aux = xa_vr' .* dx .+ 0.5 .* xa2' .* (dv[:,:,r] .-dx .* (dx_dvr ./ ndx.^2)')
                hess[r,j] += sum([dot(dv[:,k,j]./ndx[k],aux[:,k]) for k in 1:Q] .* wQ)
            end
        end
    end
    return hess
end

function local_HXS(type, x, dx, a,da, lv, lw, v, dv, w, wQ)
	Q = length(wQ)
    hess  = zeros(lv,lw)
    xa    = x .- a
    if type == eDISP
            @einsum hess[r,j] = -da[d,k]*v[d,k,r]*w[1,k,j].*wQ[k]
    else
		ndx = [norm(dx[:,j]) for j in 1:Q]
        xa_da = [dot(xa[:,k],da[:,k]) for k in 1:Q]

        da_v = zeros(Q,lv)
        @einsum da_v[k,r] = da[d,k]*v[d,k,r]

        dx_dv = zeros(Q,lv)
        @einsum dx_dv[k,r] = dx[d,k]*dv[d,k,r]

        @einsum hess[r,j] = -w[1,k,j]*wQ[k]*(da_v[k,r]*ndx[k] +
                             dx_dv[k,r] / ndx[k] * xa_da[k])
	end
    return hess
end

function local_HSX(type, x, dx, s, ds, d2s, a, da, d2a,lv, lw, v, dv,w, wQ)
	Q = length(wQ)
    hess  = zeros(lw,lv)
    xa    = x .- a
    if type == eDISP
        @einsum hess[j,r] = -da[d,k]*v[d,k,r]*w[1,k,j].*wQ[k]
    else
		ndx = [norm(dx[:,j]) for j in 1:Q]
        xa_da = [dot(xa[:,k],da[:,k]) for k in 1:Q]

        da_v = zeros(Q,lv)
        @einsum da_v[k,r] = da[d,k]*v[d,k,r]

        dx_dv = zeros(Q,lv)
        @einsum dx_dv[k,r] = dx[d,k]*dv[d,k,r]

        @einsum hess[j,r] = -w[1,k,j]*wQ[k]*(da_v[k,r]*ndx[k] +
                             dx_dv[k,r] / ndx[k] * xa_da[k])
    end

    return hess
end

function local_HSS(type, x, dx, a, da, d2a, lw, w, wQ)
	Q = length(wQ)
    hess  = zeros(lw,lw)
    xa    = x .- a
	ndx   = (type == eDISP ? fill(1.0, Q) : [norm(dx[:,j]) for j in 1:Q])

    @einsum hess[r,j] = (da[d,k]^2-xa[d,k]*d2a[d,k])*w[1,k,r]*w[1,k,j]*wQ[k]*ndx[k]
    return hess
end

function update_nodes!(meshX, meshS, step)
	nX = length(vec(meshX.nodes))
	nS = length(vec(meshS.nodes))
	for j in 1:meshX.d
		meshX.nodes[j,:] += step[j:meshX.d:nX]
	end
	for j in 1:meshS.d
		meshS.nodes[j,:] += step[nX + j:meshS.d:end]
	end
end

function get_moving_index(mx, ms, vars)
	m = length(vec(mx.nodes))
	if vars == eMoveS
		id = m .+ ms.freeColNodesList
	elseif vars == eMoveX
	  	id = mx.freeColNodesList
	else
		id = vcat(mx.freeColNodesList, m .+ ms.freeColNodesList)
	end
	return id
end

function solver_step!(it, sInfo, mX, mS, C, G, H,zw,
					  penON = false, mu=0.0, printIO = false)

	ids    = get_moving_index(mX, mS, sInfo.vars)
	xnodes = deepcopy(mX.nodes)
	snodes = deepcopy(mS.nodes)
	Hmove  = H[ids,ids]
	Gmove  = G[ids]

	sigma  = sInfo.sigma
	if (rank(Hmove) != size(Hmove,2) || iszero(det(Hmove))) && cond(Hmove) > 1.e16
		step = -Gmove
	else
		HF     = factorize(Hmove)
		step   = -1.0 .* ( Hmove \ Gmove)
		dotSG0 = dot(step,Gmove)
		if dotSG0 > 0.0
			scale = sInfo.name == eZHANG ? diag(Hmove) : fill(1.0, length(Gmove))
			step  = -Gmove ./ scale
		end
	end
	dotSG0    = dot(step,Gmove)
	fullStep      = zeros(length(vec(mX.nodes)) + length(vec(mS.nodes)))
	fullStep[ids] = step
	alpha         = 1.0

	update_nodes!(mX, mS, fullStep)
	f1     = get_disparity(mX, mS, C ,penaltyON=penON,mu=mu, zw=zw)
	G1,_,_ = update_G(sInfo.fobj, sInfo.vars, mX, mS, C,mu, zw)
	dotSG1 = dot(step,G1)

	# BACKTRACKING PARAMETERS
	btIter = 0
	######################
	gNorm  = norm(Gmove)
	normS  = norm(step)
	ds_0   = check_sign_ds(mX, mS, C, zw.nodes)
	nX     = length(vec(mX.nodes))
	nS     = length(vec(mS.nodes))

	gOK   = (sInfo.name == eZHANG ? dotSG1 >= sigma * dotSG0 : true)
	#(penON) && (gOK = true)
	fOK   = (sInfo.name == eNOLS ? true : f1 <= sInfo.c + dotSG0 * sInfo.delta * alpha)
	sOK   = ds_0
	lsON  = (isnan(f1) || isinf(f1) || !fOK || !gOK || !sOK )
	if printIO && sInfo.vars == eMoveXS
		@printf(" LS %s f1 %.2e, f0 %.2e, |f1 - f0| %.2e, ||STEP|| %.2e, LINESEARCH F %s, G %s DS %s (PEN %s MU %.2e)\n",
						sInfo.name, f1, sInfo.c, abs(f1-sInfo.c), normS, fOK, gOK, sOK, penON, mu)
	end

	while (lsON && btIter <= sInfo.ls)
		mX.nodes = deepcopy(xnodes)
		mS.nodes = deepcopy(snodes)
		alpha   *= sInfo.rho
		update_nodes!(mX, mS, alpha .* fullStep)
		f1       = get_disparity(mX, mS, C ,penaltyON=penON,mu=mu, zw=zw)
		G1,_,_   = update_G(sInfo.fobj, sInfo.vars, mX, mS, C,mu, zw)
		dotSG1   = dot(step,G1)
		normS    = alpha * norm(step)
		btIter  += 1

		gOK  = (sInfo.name == eZHANG ? dotSG1 >= sigma * dotSG0 : true)
		fOK  = (sInfo.name == eNOLS ? true : f1 <= sInfo.c + dotSG0 * sInfo.delta * alpha)
		sOK  =  check_sign_ds(mX, mS, C, zw.nodes)
		lsON = (isnan(f1) || isinf(f1) || !fOK || !gOK || !sOK )
		if printIO
			@printf(" ---> BT %s, btIter %2d,alpha %0.2e,f1 %.2e, f0 %.2e, |f1 - f0| %.2e, ||STEP|| %.2e, LINE SEARCH F %s, G %s %.7e < %.7e DS %s (PEN %s MU %.2e)\n",
							sInfo.name, btIter, alpha, f1, sInfo.f, abs(f1-sInfo.f), normS, fOK, gOK,dotSG1, sigma * dotSG0, sOK, penON, mu)
		end
	end
	if lsON
		mX.nodes = deepcopy(xnodes)
		mS.nodes = deepcopy(snodes)
		(!penON) && (sOK = ds_0)
	end
	sInfo.f = f1
	if sInfo.name == eARMIJO
		sInfo.c = sInfo.f
	elseif sInfo.name == eZHANG
		aux      =  sInfo.q * sInfo.eta + 1.0
		sInfo.c = (sInfo.q * sInfo.eta * sInfo.c + sInfo.f) / aux
		sInfo.q = aux
	end

	return sOK, btIter, lsON, step .* alpha
end

function check_sign_ds(meshX, meshS, curve, z)
	ds   = evaluate_object(meshS, z,order=1,all=false)
	sign = 1
	if any(ds[1,:] .< 0.0)
		sign = all(ds[1,:] .< 0.0) ? -1 : eGEOMERR
   	end
	return curve.dir == sign
end

function reset_LS!(sInfo, f)
	sInfo.c = f
	sInfo.f = f
	sInfo.q = 1.0
end

function BTLSNewton(sInfo::NLS_pars, meshX::meshInfo, meshS::meshInfo, curve::curveInfo;
                	penaltyON = false::Bool, mu=0.0::dbl, zw = nothing::Union{Nothing,gaussZW}, printIO = false::Bool)


    stopG = stopS = stopLS = stopF = false
    disp  = gNorm = normS = maxG = 0.0
    totBT = itn   = 0

	zw = gaussZW(eGLL, curve.d*2 * max(meshX.p, meshS.p))
	signDS = check_sign_ds(meshX, meshS, curve, zw.nodes)
	if !signDS
		throw(@error " INSIDE ORIGINAL CURVE PARAMETRIZATION LOOPS FWD - BWD. FIX IT !!!!!!!")
	end
	dsOK     = true
	disp0    = get_disparity(meshX, meshS, curve,penaltyON=penaltyON,mu=mu, zw=zw)
	dispOri  = disp0
	reset_LS!(sInfo,disp0)

	nX = length(vec(meshX.nodes))
    nS = length(vec(meshS.nodes))
	if printIO
		@info " --- START: itMAX = $(sInfo.it), line searches: $(sInfo.ls) , objective $(sInfo.fobj), norm $(sInfo.norm) optimization: deg(x)= $(meshX.p) deg(s)= $(meshS.p) elements = $(meshX.nE) penalty on ? $penaltyON (mu = $mu) "
	end

	itn = 0
	while (itn < sInfo.it && !stopLS && !stopS && !stopG && dsOK != eGEOMERR && !stopF)
        grad,hess,grad_disp,grad_barrier =
                	update_GH(sInfo.fobj,sInfo.vars,meshX, meshS, curve,mu, zw)
		cond_H        = cond(hess)
		xsID          = get_moving_index(meshX, meshS, sInfo.vars)
		gNorm         = my_norm(grad[xsID], sInfo.norm)
		gMAX          = my_norm(grad[xsID], eLinf)
        gDisp_Norm    = my_norm(grad_disp[xsID], sInfo.norm)
		gBarrier_Norm = my_norm(grad_barrier[xsID], sInfo.norm)
		if isnan(gNorm) || isinf(gNorm)
			stat = eGEOMERR
			break
		end
		if itn == 0 && printIO
			@printf("\nMIN_L2 it = %4d, btIT %2d, mu = %1.1e, ||G||_0 %3.3e,||G||_inf %3.3e, ||G_obj|| %3.3e, ||G_barrier|| %3.3e,  cond(H) %2.2e, INITIAL f %3.3e   START OPTIMIZATION \n",
                    	itn,0, mu, gNorm, gMAX, gDisp_Norm, gBarrier_Norm, cond_H, disp0)
		end

		stopG  = gNorm < sInfo.gTOL
		(stopG) && (break)
		dsOK, btIter,stopLS,step = solver_step!(itn, sInfo, meshX, meshS, curve, grad, hess, zw, penaltyON, mu,printIO)

		normS      = my_norm(step,sInfo.norm)
		(normS < sInfo.sTOL) && (stopS = true)

		disp     = get_disparity(meshX, meshS, curve,penaltyON=penaltyON,mu=mu, zw=zw)

		totBT   += btIter
		itn     += 1
		if printIO
			@printf("\n MIN_L2 it = %4d, btIT %2d, mu = %1.1e, ||G||_0 %3.7e,||G||_inf %3.3e, ||G_obj|| %3.3e, ||G_barrier|| %3.3e, ||step|| %.3e, cond(H) %2.2e, f1 %3.3e, f0 %3.3e, |f1-f0| %3.3e\n",
				itn, btIter, mu, gNorm,gMAX,gDisp_Norm, gBarrier_Norm, normS, cond_H, disp, disp0, abs(disp-disp0))

		end
		#stopF = abs(disp0 - disp) < sInfo.fTOL * disp
		disp0 = disp
    end
    stat = eSUCCESS
	if !dsOK
		printIO && (@printf("\n\n----------------> !!!! ATTENTION :: LS FAILS: CURVE GOES FWD-BWD, RESTORE LAST ACCEPTED MESH AND CALL WITH PENALTY \t"))
		stat = eTRYPENALTY
	elseif stopG
        printIO && (@printf("\n\n---------------->  STOP CRITERIA: grad  || %.2e || < %.2e\t", gNorm, sInfo.gTOL))
		stat = eGRADTOL
    elseif stopS
        printIO && (@printf("\n\n---------------->  STOP CRITERIA: step  || %.2e || < %.2e\t", normS, sInfo.sTOL))
		stat = eSTEPTOL
	elseif stopF
        printIO && (@printf("\n\n---------------->  STOP CRITERIA: f1 - f0 || %.2e || < %.2e\t", disp - disp0, sInfo.fTOL * disp))
		stat = eFUNTOL
    elseif stopLS
        printIO && (@printf("\n\n---------------->  STOP CRITERIA: STUCK LINE-SEARCHING \t"))
		stat = eMAXLS
	else
        printIO && (@printf("\n\n---------------->  STOP CRITERIA: REACHED MAXIMUM NUMBER OF ITERATIONS \t"))
		stat = eMAXITERS
    end
	if printIO
		@printf("  <----------------")
		@printf("\n  TOTALS: p %d, q %d, n %d,fobj %s, iters %d, ls %d, ||grad|| %.2e, ||step|| %.2e, final f %.2e, initial f %.2e,  mu %.2e (PENALTY %s)\n\n",
            		meshX.p,meshS.p,meshX.nE,sInfo.fobj, itn, totBT, gNorm, normS, disp, dispOri, mu, penaltyON)
	end
	return stat,itn, totBT, gNorm
end

function partitioned_mesh_optimization(CADfile::String,lsZ::NLS_pars, pX::Int,pS::Int,nE::Int, ids::Vector{Int}, ctime::Vector{Float64},iters::Vector{Int}, nx:: Array{Float64, 3},ns:: Array{Float64, 3})
	de = zeros(nE) ; it = zeros(Int,nE)
	aux = EGADS_curve(CADfile)
	curve, cid = aux.curves, aux.cids
	pivS = [1:pS+1;] ;	pivX = [1:pX+1;]
	for j = 1:length(ids)
		C = curve[ids[j]]
		t0 = time()
		mxE,msE = create_partitioned_mesh(C,pX,pS,nE)
		info    = egads.getRange(C.ego)
		arc     = egads.arcLength(C.ego,info.range[1], info.range[2])
		px      = [1:pX+1;] ; ps = [1:pS+1;]
		ctt     = filter(x -> !(x in C.plane), [1,2,3])
		ctime[j] = @elapsed for e = 1:nE
			de[e],_,it[e],_,_ = mesh_optimization(lsZ,mxE[e],msE[e],C)
			ns[j,:,pivS .+ pS .* (e-1)] = msE[e].nodes
			if C.d != 3
				nx[j,C.plane,pivX .+ pX .* (e-1)] .= mxE[e].nodes
				for d = 1:length(ctt)
					nx[j,ctt[d],pivX .+ pX .* (e-1)] .= C.cttPlane[d]
				end
			else
				nx[j,:,pivX .+ pX .* (e-1)] .= mxE[e].nodes
			end
		end
		iters[j] = maximum(it)
	end
end


function distributed_mesh_optimization(CADfile::String,lsZ::NLS_pars, pX::Int,pS::Int,nE::Int, ids::Vector{Int}, ctime::Vector{Float64},iters::Vector{Int}, nx:: Array{Float64, 3},ns:: Array{Float64, 3})
    aux = EGADS_curve(CADfile)
    curve, cid = aux.curves, aux.cids
    for j = 1:length(ids)
        C       = curve[ids[j]]
        t0      = time()
        mxE,msE = create_mesh(C,pX,pS,nE)
        info    = egads.getRange(C.ego)
        arc     = egads.arcLength(C.ego,info.range[1], info.range[2])
        ctt     = filter(x -> !(x in C.plane), [1,2,3])
        ctime[j] = _,_,iters[j],_,_ = mesh_optimization(lsZ,mxE,msE,C)
		ns[j,:,:] = msE.nodes
        if C.d != 3
        	nx[j,C.plane,:] .= mxE.nodes
            for d = 1:length(ctt)
              	nx[j,ctt[d],:] .= C.cttPlane[d]
            end
        else
        	nx[j,:,:] .= mxE.nodes
        end
    end
end


function mesh_optimization(sInfo::NLS_pars, meshX::meshInfo, meshS::meshInfo, curve::curveInfo;
	                       zw = nothing, printIO = false::Bool)

   	nPEN  = 0
	gN = 0.0
	zw = gaussZW(eGLL, curve.d*2 * max(meshX.p, meshS.p))

	disp0    = get_disparity(meshX, meshS, curve,zw=zw)
	(length(free_nodes_XS(meshX, meshS)) == 0) && (return disp0,eSTEPTOL, 0,0,0,gN)
	stat, tIT, tLS, gN = BTLSNewton(sInfo, meshX, meshS, curve; penaltyON = false, mu = 0.0, zw = zw, printIO = printIO)
	disp = get_disparity(meshX, meshS, curve,zw=zw)
	(stat != eTRYPENALTY) && (return disp,stat, tIT, tLS, nPEN, gN)

    mu       = -disp0# * 1.e-2
	backIT   = sInfo.it
	sInfo.it = 20
	for it in 1:(maxMU=6)
        if it == maxMU
			mu = 0.0
			sInfo.it = backIT
	    end
        printIO && @info " *********** ACTIVATE LOG BARRIER -> MU = $mu, it $it/$maxMU"

        stat, a,b,gN = BTLSNewton(sInfo, meshX, meshS, curve; penaltyON = true, mu = mu, zw = zw, printIO = printIO)
		nPEN += 1
        tIT  += a
        tLS  += b
        if stat == eGEOMERR
            @error "!! Log barrier failed! Newton returned with bad status !"
            break
        end
        mu *= 1.e-1
	end
	disp = get_disparity(meshX, meshS, curve,zw=zw)
	return disp,stat, tIT, tLS, nPEN, gN
end


"""
	Returns the condition (by name) why Newton stopped
"""
function get_stat_name(stat::Int)
	if stat == eMAXITERS
		return("it-max")
	elseif stat == eGRADTOL
		return("g-tol")
	elseif stat == eSTEPTOL
		return("s-tol")
	elseif stat == eMAXLS
		return("ls-max")
	elseif stat == eFUNTOL
		return("f-tol")
	else
		return("FAILURE")
	end
end
