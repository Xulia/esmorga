#=
 ******************************************************************************
 * ESMORGA:Exploiting Superconvergence in Meshes for Optimal Representations  *
 *                                        of the Geometry with high Accuracy  *
 *                                                                            *
 * This file is part of the esmorga module: geometry functions & structures   *
 * Written by Xulia Docampo @ BSC-CNS                                         *
 *                                                                            *
 * Licensed under the GNU Lesser General Public License, version 2.1          *
 * See http://www.opensource.org/licenses/lgpl-2.1.php                        *
 ******************************************************************************
 =#
struct gaussZW
   type    :: String
   Q       :: int
   nodes   :: vDbl
   weights :: vDbl
   function gaussZW(tQ::String, Q::int)
	   if tQ == eGL
		   z,w = GQ.gausslegendre(Q)
	   elseif tQ == eUNIF
	       z = [-1.0 + 2.0 / (Q-1) * j for j = 0:Q-1]
	       w = fill(1.0 / Q, Q)
	   else
	       z,w = GQ.gausslobatto(Q)
	   end
	   return new(tQ, Q, z,w)
   end
end

"""
Computes either the L2 or Linf norm during optimization (for gradient, Newton step, etc.)
"""
my_norm(vec,type) = type == eL2 ? norm(vec) : maximum(abs.(vec))

"""
Creates the mesh shape functions
Params:
-------
p:
	polynomial degree
eval_pts:
 	evaluation points [-1,1]
nders (optional):
	choose information on the basis function: stores by default up to 3rd order

Return:
--------
phi: a 3D array [n,q,p+1]:

n -> shape || derivative: 1=phi, 2=dphi, 3=d2phi, etc,

q = position of the evaluation point,

p+1= position of the shape basis function
"""
function create_shape_funs(p,eval_pts, nders=3)
    J   = [poly_jacobi(i,0.0,0.0) for i = 0:p]
    zwR = gaussZW(eGLL, p + 1)

    Q   = length(eval_pts)
    phi = zeros(nders + 1, Q, p + 1)
    V   = zeros(p+1,p+1)
    P   = zeros((p+1, Q))
    [ V[i,:] = J[i].(zwR.nodes) for i = 1:p+1]
    for j in 1:nders+1
        (j > 1) && (J = [derivative(J[i]) for i = 1:p+1])
        [ P[i,:] = J[i].(eval_pts) for i = 1:p+1]
        phi[j,:,:] = transpose(V\P)
    end
    return phi
end


"""
Structure with information about the underlying curve

Members:
--------
d:
    dimension (space)
t0, t1:
    parametric interval where the curve is defined
a,b:
    curve parameters (used for analytic data) such as, radius, frequency
id:
    curve associated name. See constants.jl for analytic curves
closed:
    true / false if the curve is closed
nE:
    number of elements associated to the curve partition
dir:
    1 = counter clock-wise (FWD), -1= clockwise (BWD)
"""
mutable struct curveInfo
	d        :: int
    t0       :: Number
    t1       :: Number
    ego      :: Any
	tess     :: vDbl
    a        :: Number # FOR CIRCLE a = radius
    b        :: Number # FOR CIRCLE b = frequency
    id       :: int
    closed   :: Bool
    nE       :: int
	dir      :: int
	plane    :: vInt
	cttPlane :: vDbl
	function curveInfo(d::int,t0::Number,t1::Number;
		               closed=false::Bool,
					   nE=0::int,
					   a=0.0::Number,# FOR CIRCLE a = radius,
					   b=0.0::Number,  # FOR CIRCLE b = frequency,
					   ego=nothing::Union{typeof(ego),Nothing},
		               tess=dbl[]::vDbl,
					   plane=Int[]::vInt,
					   cttPlane=dbl[]::vDbl,
					   id =(-1)::int,
					   dir = 1::int)
			return new(d, t0, t1, ego, tess,a, b, id, closed, nE, dir, plane, cttPlane)
	end
end

function EGADS_curve(cadfile::String;egadsTess = false::Bool,xyz_cond =(0,0.0)::Tuple{int,dbl}, curveID = 0::Int)

	context = egads.Context()
	lib     = splitext(basename(egads.C_egadslib))[1]
	ftype   = splitext(basename(cadfile))[2]
	if ftype == ".lite" && lib in ["libegadslite","egadslite"]
		datafile = UInt8[]
		file = open(cadfile,"r")
		readbytes!(file,datafile,2^32)
		close(file)
		model = egads.importModel(context,datafile)
	elseif ftype in [".egads",".stp"] && lib in ["libegads","egads"]
		model = egads.loadModel(context,0,cadfile)
	else
		@error " Type CAD $cadfile and lib $lib incompatible for loading EGADS_curves !! "
	end
	aux     = egads.getTopology(model)

	if length(aux.children) != 1
	    @error "I am expecting one single body but got $(length(aux.children))"
	    return
	end
	body = aux.children[1]
	tess = dbl[]

	if egadsTess
	    bbox     = egads.getBoundingBox(body)
	    box_size = maximum(bbox[4:6] .- bbox[1:3])
	    ppars    = [0.25*box_size, 0.01*box_size, 20.0 ]
		tess     = egads.makeTessBody(body, ppars)
	end
	edges = egads.getBodyTopos(body, egads.EDGE)
	degen = fill(1, length(edges))
	for e = 1:length(edges)
		info = egads.getInfo(edges[e])
		if info.mtype == egads.DEGENERATE
			degen[e] = 0
			continue
		end
		ran  = egads.getRange(edges[e])
		t01  = 0.5 * sum(ran.range)
		pm   = egads.evaluate(edges[e],t01).X
		if (xyz_cond[1] != 0 && pm[int(xyz_cond[1])] > xyz_cond[2]) # if the model is symmetric about a plane, ie, y-axis, no need to compute every curve twice
			degen[e] = 0
		end
	end

    piv = curveID > 0  ? [curveID] : findall(x-> x == 1, degen)
    curves = Array{curveInfo}(undef,length(piv))
    for e = 1:length(piv)
		ee = edges[piv[e]]
		info = egads.getRange(ee)
		idx  = egads.indexBodyTopo(body, ee)
	    ts   = isempty(tess) ? dbl[] : egads.getTessEdge(tess, idx)[2]
		if info.range[2] < info.range[1]
			reverse!(ts) ; reverse!(info.range)
		end
		xyz1 = egads.evaluate(ee, info.range[1]).X
		xyz2 = egads.evaluate(ee, info.range[2]).X
		isClosed = (info.periodic == 1 && norm(xyz1 .- xyz2) < 1.e-13)
	    nt  = 10
	    xyz = zeros(3,nt)
		for j = 1:nt
	        t        = info.range[1] + (j-1) * (info.range[2]-info.range[1]) / (nt-1)
	        xyz[:,j] = egads.evaluate(ee, t).X
	    end
		plana     = [all(abs.(xyz[j,1] .- xyz[j,:]) .< 1.e-14) for j = 1:3]
	    aux1      = findall(z->z == false, plana)
		aux2      = length(aux1) == 3 ? 1 : findall(z->z == true, plana)

		curves[e] = curveInfo(length(aux1), info.range[1],info.range[2];
	                closed = isClosed,ego = ee, tess = ts, plane = aux1,
					cttPlane = vcat([xyz[aux2,1]]...))
	end
    return (context = context, model = model, curves = curveID > 0 ? curves[1] : curves , cids = piv)
end


"""
Structure with mesh information: either parametric (s) or physical (x)

mesh = sum_{j=1}^{p+1} x_j * N^p_j   p = degree, N = basis

Members:
--------
nodes:
    the x_j (s_j) as a flat 2D vector.
    Nodes aren't repeated: element 1 = x_1,...,x_{p+1} ; element 2: x_{p+1},...,x{2p+1}, etc
nE:
    number of elements
d:
    mesh space
phi:
    associated shape functions
freeNodesList:
    nodes that can move during optimization. When constrained, it will fix the interfaces
freeColNodesList:
    same freeNodesList but collapsed matrix (used to set the optimization matrix)
eleNodes:
    node ID corresponding to each element. EG: eleNodes[1] = 1,...,p+1 ; eleNodes[2] = p+1,...,2p+1
eleCoolNodesList:
    same node ID but collapsed matrix (used to set the optimization matrix)
loop:
    if closed curve (true) it will identify 1st and last node as a single node
"""
mutable struct meshInfo
    nodes            :: Matrix{dbl}
    p                :: int
    nE               :: int
    d                :: int
    phi              :: Array{dbl,3}

    freeNodesList    :: vInt
    freeColNodesList :: vInt

    eleNodes         :: Matrix{int}
    eleColNodes      :: Matrix{int}
    loop             :: vInt
end


"""
Returns the curve name (analytic, for CAD returns unknown)
"""
function get_curve_name(id::int)
    if id == eCIRCLE return "Circle"
    elseif id == eSEMICIRCLE return "Semi-Circle"
    elseif id == eCIRCLEPOL return "Circle-poly"
    elseif id == eSPIRAL    return "Spiral"
    elseif id == eARCSPIRAL return "ArcLength-Spiral"
    elseif id == ePETALS return "Petals"
    elseif id == eCUBIC return "Cubic"
    elseif id == eNACA return "NACA"
    elseif id == eSPIRAL3D return "Spiral_3D"
    elseif id == eARCHYTAS return "Archytas"
    elseif id == eHELIX  return "Helix"
    elseif id == eSPHERE  return "Sphere"
    else return "Unknown"
    end
end


"""
computes the disparity || x - a o s ||_sigma

Params:
-------
meshX, meshS:
    physical and parametric meshes
curve:
    the associated curve (a)
penaltyON (default false):
    if log barrier is on: compute the penalty term: int mu * log(1/s')
mu (default 0):
    the factor in the log barrier term
zw (default nothing):
    the quadrature nodes & weights

Returns:
--------
    the disparity value
"""
function get_disparity(meshX::meshInfo, meshS::meshInfo, curve::curveInfo;
	                   penaltyON=false::Bool,mu=0.0::dbl, zw=nothing::Union{Nothing,gaussZW})
    (zw == nothing) && (zw = gaussZW(eGLL,curve.d*2 * max(meshX.p, meshS.p) ) )
    x = evaluate_object(meshX, zw.nodes,order=1,all=true)
    s = evaluate_object(meshS, zw.nodes,order=1,all=true)
    a = evaluate_object(curve, s[1,1,:],order=1,all=true)
    ndx = [norm(x[2,:,j]) for j in 1:zw.Q * meshX.nE ]
    fun = 0.0
	err = [norm(x[1,:,j].-a[1,:,j]) for j = 1:zw.Q * meshX.nE]
    for e in 1:meshX.nE
        piv  = [(e-1)*zw.Q+1:1:e*zw.Q;]
        fun += 0.5 * sum(ndx[piv] .* err[piv].^2 .* zw.weights)
        aux  = [max(1.e-324,s[2,1, piv[j]] + 1.e-23) for j in 1:zw.Q]
        !penaltyON && continue
        penalty = mu * sum(log.(aux).* zw.weights) ## !!!WARNING ESTO TENDRIA QUE SER INFTY SI AUX < 0 EN AL MENOS UN PUNTO
        fun += penalty
    end
    return fun
end


function initial_disparity(C::curveInfo, nE::Int, pX::Int,pS::Int; meshType = esmorga.eARCLENGTH::String, plotStuff=false::Bool,arcNorm=false::Bool)
	info  = egads.getRange(C.ego)
	arc   = arc   = arcNorm ? egads.arcLength(C.ego,info.range[1], info.range[2]) : 1.0
	xI,sI = create_mesh(C, nE, pX,pS, meshType=meshType);
	if plotStuff
		x,a   = sample_curves(xI,sI,C,make3d = true)
		ax.plot(a[1,:],a[2,:],a[3,:])
		ax.plot(x[1,:],x[2,:],x[3,:])
		zw = gaussZW(eGLL,2)
		x,a   = sample_curves(xI,sI,C,make3d = true, z = zw.nodes)
		ax.scatter(x[1,:],x[2,:],x[3,:])
	end
	return sqrt(get_disparity(xI,sI,C)) / arc
end


"""
Evaluates a curve or a mesh at the given points
Params:
-------
T:
    curve: analytic or egads object
xx:
    parameter
order (default=0):
    for derivatives: =1 (1st der), = 2 (2nd der), etc
id (default = -1):
    if >0, evaluates only at that element
all (default = true)
    if false and order > 0, returns only the derivative of that order
Returns:
    position (and derivatives)
"""
function evaluate_object(T::Union{curveInfo,meshInfo}, xx::Union{dbl,vDbl};
	                     order=0::int, id=(-1)::int, all = true::Bool)
	x = vcat(xx...)
	if typeof(T) == meshInfo
		T.phi = create_shape_funs(T.p, x)
		!isempty(T.loop) && (T.nodes[:,T.loop[1]] .= T.nodes[:,T.loop[2]])
        	n     = id < 1 ? T.nE : n = 1
        	Q     = length(x)
        	val   = zeros((order + 1, T.d, Q*n))
        	ye    = zeros( order + 1, T.d,   Q)
        	basis = T.phi[1:order+1,:,:]
        	for e in 1:n
           		 nID  = id >= 1 ? T.eleNodes[id,:] : T.eleNodes[e,:]
            		piv  = [(e-1) * Q+1:1:e*Q;]
            		locN = T.nodes[:,nID]
            		@einsum ye[j,d,q] = locN[d,p] * basis[j,q,p]
            		val[:,:,piv] = ye
        	end
	elseif typeof(T.ego) == ego
		n    = length(x)
		vals = zeros(3,3,n)
		for j = 1:n
			res = egads.evaluate(T.ego, x[j])
			vals[1,:,j] .= res.X
			vals[2,:,j] .= res.dX
			vals[3,:,j] .= res.ddX
	    end
		val = vals[:,T.plane,:]
	else
		val = evaluate_curve(T, x, order)
	end
	if !all || order == 0
        	return val[order + 1,:,:]
    	else
        	return val
    	end
end


function evaluate_curve(curve::curveInfo, t::vDbl, ders::int)
    isempty(t) && (t = [curve.t0, curve.t1])
    val = zeros(ders+1,curve.d, length(t))
    if curve.id == ePOLY2D
        val[1,2,:] = t.^2
        val[1,1,:] = t
        if ders >= 1
            val[2,2,:]  = 2.0 .* t
            val[2,1,:] .= 1.0
            if ders >= 2
                val[3,2,:].= 2.0
                val[3,1,:].= 0.0#2.0
            end
        end

    elseif curve.id == eARCSPIRAL
        s1  = log.(1 .+ curve.t1 .* curve.b / (curve.a * sqrt(curve.b^2 + 1)) ) / curve.b
        spi = curveInfo(curve.d,curve.t0, s1; closed = curve.closed, a=curve.a, b=curve.b, id = eSPIRAL)
        arg_log = 1 .+ t .* curve.b / (curve.a * sqrt(curve.b^2 + 1))
        tpos = [1:1:length(t);]
        if any(arg_log .< 0.0)
            tneg = findall(x->x < 0.0,arg_log)
            tpos = deleteat!(tpos, tneg)
            arg_log[tneg] .= 1.e-324
            val[tneg]     .= Inf
        end
        s    = log.(arg_log[tpos]) / curve.b
        valP = evaluate_curve(spi, s, ders)
        if ders >= 1
            ## arc length s = (e^bt - 1)*a*sqrt(b^2 + 1) / b
            # ds/ dt = |a'(t)| => dt/ds = 1 / |a'(t)|
            dtds = curve.a * sqrt(curve.b^2 + 1) .*exp.(curve.b .* s)
            nda  = [norm(valP[2,:,j]) for j = 1:length(s)]
            if sum(abs.(dtds - nda)) > 1.e-12
                print(" WE HAVE THE WRONG ARCLENGTH FUNCTION ! \n")
                print(" ds - |a'| = ",sum(abs.(dtds - nda)),sum(abs.(dtds - nda)) > 1.e-15,"\n")
            end
            if ders >= 2
                dad2a = [dot(valP[2,:,j], valP[3,:,j]) for j = 1:length(s)]
                @einsum valP[3,k,j] = valP[3,k,j] / nda[j]^2 - valP[2,k,j] * dad2a[j] / nda[j]^3
            end
            @einsum valP[2,k,j] = valP[2,k,j] / nda[j]
        end
        val[:,:,tpos] .= valP
    elseif curve.id == eCIRCLE || curve.id == eSEMICIRCLE# CIRCLE
        val[1,1,:] = curve.a * cos.(curve.b .* t)
        val[1,2,:] = curve.a * sin.(curve.b .* t)
        if ders >= 1
            val[2,1,:] = -curve.a * curve.b * sin.(curve.b .* t)
            val[2,2,:] =  curve.a * curve.b * cos.(curve.b .* t)
            if ders >= 2
                val[3,1,:]= -curve.a * curve.b^2 * cos.(curve.b.* t)
                val[3,2,:]= -curve.a * curve.b^2 * sin.(curve.b.* t)
                if ders == 3
                    val[4,1,:]=  curve.a * curve.b^3 * sin.(curve.b .* t)
                    val[4,2,:]= -curve.a * curve.b^3 * cos.(curve.b .* t)
                end
            end
        end
    elseif curve.id == eCIRCLEXY
        val[1,1,:] = (1.0 .- t.^2) ./(1.0.+t.^2)
        val[1,2,:] = (2.0.*t)      ./(1.0.+t.^2)
        if ders >= 1
            val[2,1,:] = -4.0 .* t           ./ (1.0 .+t.^2).^2
            val[2,2,:] = -2.0 .* (t.^2.0.-1) ./ (1.0 .+t.^2).^2
            if ders >= 2
                val[3,1,:] = 4.0 .*(3.0 .*t .-1.0) ./ (1.0.+t.^2).^3
                val[3,2,:] = 4.0 .*t.*(t.^2 .-3.0) ./ (1.0.+t.^2).^3
            end
        end
    elseif curve.id == eNACA

        signT      = sign.(t)
        t1         = signT .* t

        val[1,1,:] = curve.a .*t1 .^2
        val[1,2,:] = 5.0.*signT.*curve.a.*curve.b.*
                              (0.2969.*t1 .- 0.126.*t1.^2 - 0.3516.*t1.^4 .+ 0.2843.*t1.^6 .- 0.1036.*t1.^8)
        if ders >= 1
            val[2,1,:] = 2.0.*curve.a.*t1.*signT
            val[2,2,:] = 5.0.*curve.a.*curve.b.*
                                          (0.2969 .- 2.0.*0.126.*t1 - 4.0.*0.3516.*t1.^3 + 6.0.*0.2843.*t1.^5 .- 8.0.*0.1036.*t1.^7)
            if ders >= 2
                val[3,1,:].=2.0.*curve.a
                val[3,2,:] =5.0.*signT.*curve.a.*curve.b.*
                                             (-2.0.*0.126 .- 12.0.*0.3516.*t1.^2 .+ 30.0.*0.2843.*t1.^4 .- 56.0.*0.1036.*t1.^6)
            end
        end
    elseif curve.id == eCIRCLEPOL
        p  = 3
        u  = t.^p .* pi
        val[1,1,:]= curve.a * cos.(u)
        val[1,2,:]= curve.a * sin.(u)
        if ders >= 1
            du = pi * p .* t.^(p - 1)
            val[2,1,:]= -curve.a .* sin.(u).*du
            val[2,2,:]=  curve.a .* cos.(u).*du
            if ders >= 2
                d2u = t.^(p - 2).* pi * p * (p - 1)
                val[3,1,:]= -curve.a.*cos.(u).*du.^2 -curve.a*sin.(u).*d2u
                val[3,2,:]= -curve.a.*sin.(u).*du.^2 +curve.a*cos.(u).*d2u
                if ders == 3
                    d3u = t.^(p - 3).*pi*p*(p - 1)*(p - 2)
                    val[4,1,:]= (curve.a.*du.^3)  .*sin.(u)-(2.0*curve.a.*du.*d2u).*cos.(u)
                                -(curve.a.*du.*d2u).*cos.(u)-(curve.a.*d3u)        .*sin.(u)
                    val[4,2,:] =-(curve.a.*du.^3)  .*cos.(u)-(2.0*curve.a.*du.*d2u).*sin.(u)
                                -(curve.a.*du.*d2u).*sin.(u)+(curve.a.*d3u)        .*cos.(u)
                end
            end
        end
    elseif curve.id == eLEMNISCATA
        ct = cos.(t)
        st = sin.(t)

        c2t = cos.(2.0.*t)
        s2t = sin.(2.0.*t)

        c4t = cos.(4.0.*t)
        s4t = sin.(4.0.*t)

        den = 1.0.+ st.^2
        val[1,1,:] = curve.a .* ct ./ den
        val[1,2,:] = curve.a .* ct .* st ./ den
        if ders >= 1
            den = c2t.-3.0
            val[2,1,:]= .-2.0 * curve.a .* st.*(c2t .+5.0) ./ den.^2
            val[2,2,:]=   2.0 * curve.a .* (3.0 .*c2t .-1.0) ./ den.^2
            if ders >= 2
                val[3,1,:]= curve.a .* ct .*(44.0 .*c2t .+c4t .-21.0)./den.^3
                val[3,2,:]= 4.0 .*curve.a .* s2t .*(3.0 .*c2t .+7.0) ./den.^3
            end
        end
    elseif curve.id == eSPIRAL || curve.id == ePETALS
        if curve.id == eSPIRAL
            r   = curve.a .* exp.(curve.b .* t)
            dr  = curve.b .* r
            d2r = curve.b^2 .* r
            d3r = curve.b^3 .* r
        else
            r   = sin.(curve.a .* t) .* exp.(curve.b.*t)
            dr  = curve.a * cos.(curve.a .* t) .* exp.(curve.b .* t).+ r .* curve.b
            d2r = curve.b .* dr - curve.a^2 .* r  +
                  curve.a .* curve.b * cos.(curve.a .* t) .* exp.(curve.b .* t)
            d3r = curve.b .* d2r - curve.a^2 .* dr - curve.a^2 .* curve.b * r +
                  curve.a * curve.b^2 *cos.(curve.a.*t).*exp.(curve.b.*t)
        end
        val[1,1,:]= r .* cos.(t)
        val[1,2,:]= r .* sin.(t)
        if ders >= 1
            val[2,1,:]= -r .* sin.(t) .+ dr .* cos.(t)
            val[2,2,:]=  r .* cos.(t) .+ dr .* sin.(t)
            if ders >= 2
                val[3,1,:]= -r .* cos.(t) - 2.0 * dr .* sin.(t) .+ d2r .* cos.(t)
                val[3,2,:]= -r .* sin.(t) + 2.0 * dr .* cos.(t) .+ d2r .* sin.(t)
                if ders == 3
                    val[4,1,:]= -r .* cos.(t) - 2.0 * dr .* sin.(t) .+ d2r .* cos.(t)
                    val[4,2,:]= -r .* sin.(t) + 2.0 * dr .* cos.(t) .+ d2r .* sin.(t)
                end
            end
        end
    elseif curve.id == eHELIX
        turns = 4
        arc   = (curve.t1 - curve.t0) * 2.0 * pi * curve.b

        val[1,1,:] = curve.a * cos.(arc .* t .+ curve.t0)
        val[1,2,:] = curve.a * sin.(arc .* t .+ curve.t0)
        val[1,3,:] = 3.0 / (2.0 * pi * turns).* t

        if ders >= 1
            val[2,1,:]  = -curve.a * arc *sin.(arc .* t .+ curve.t0)
            val[2,2,:]  =  curve.a * arc *cos.(arc .* t .+ curve.t0)
            val[2,3,:] .=  3.0 / (2.0 * pi * turns)
            if ders >= 2
                val[3,1,:]= -curve.a * arc^2 *cos.(arc .* t .+ curve.t0)
                val[3,2,:]= -curve.a * arc^2 *sin.(arc .* t .+ curve.t0)
                if ders == 3
                    val[4,1,:]=  curve.a * arc^3 *sin.(arc .* t .+ curve.t0)
                    val[4,2,:]= -curve.a * arc^3 *cos.(arc .* t .+ curve.t0)
                end
            end
        end
    elseif curve.id == eSPIRAL3D
        c1 = 1
        c2 = 2
        c3 = 3
        val[1,c3,:] = t .* cos.(t)
        val[1,c2,:] = t .* sin.(t)
        val[1,c1,:] = t
        if ders >= 1
            val[2,c3,:] = cos.(t) .- t .* sin.(t)
            val[2,c2,:] = sin.(t) .+ t .* cos.(t)
            val[2,c1,:].= 1.0
            if ders >= 2
                val[3,c3,:] = -2.0 * sin.(t) .- t.* cos.(t)
                val[3,c2,:] =  2.0 * cos.(t) .- t.* sin.(t)
                val[3,c1,:].=  0.0

            end
        end
    elseif curve.id == eCIRCLE3D
        c1 = 2
        c2 = 3
        val[1,c1,:] = curve.a * cos.(curve.b .* t)
        val[1,c2,:] = curve.a * sin.(curve.b .* t)
        if ders >= 1
            val[2,c1,:] = -curve.a * curve.b * sin.(curve.b .* t)
            val[2,c2,:] =  curve.a * curve.b * cos.(curve.b .* t)
            if ders >= 2
                val[3,c1,:]= -curve.a * curve.b^2 * cos.(curve.b.* t)
                val[3,c2,:]= -curve.a * curve.b^2 * sin.(curve.b.* t)
            end
        end
    elseif curve.id == ePOLY3D
        val[1,1,:]  = t.^3
        val[1,3,:] .= t.^4
        val[1,2,:] .= t
        if ders >= 1
            val[2,1,:] = 3.0 .* t.^2
            val[2,3,:].= 4.0 .* t.^3
            val[2,2,:].= 1.0
            if ders >= 2
                val[3,1,:].= 6.0 .*t
                val[3,3,:].= 12.0 .*t.^2
                val[3,2,:].= 0.0
            end
        end
    elseif curve.id == eARCHYTAS
        ct  = cos.(t)
        c2t = cos.(2.0.*t)
        c4t = cos.(4.0.*t)
        c6t = cos.(6.0.*t)

        st  = sin.(t)
        s2t = sin.(2.0.*t)
        s4t = sin.(4.0.*t)
        den = 1.0 .+ st.^2
        val[1,1,:] = ct.^4   ./ den.^2
        val[1,2,:] = ct.*s2t ./den.^2
        val[1,3,:] = s2t./(sqrt(2).*den)
        den = (c2t.-3.0)
        if ders >= 1
            val[2,1,:] = 64.0*st.*ct.^3 ./ den.^3
            val[2,2,:] = .-2.0.*ct.*(20.0.*c2t.+c4t.-13.0)./den.^3
            val[2,3,:] = 2.0 .*sqrt(2.0) .* (3.0 .* c2t .-1.0) ./ den.^2
            if ders >= 2
                val[3,1,:].= .- 32.0 .* ct.^2 .*(14.0 .*c2t .+c4t .-11.0) ./ den.^4
                val[3,2,:].= .- st .*(431.0 .*c2t .+94.0 .*c4t .+ c6t .+82.0 ) ./ den.^4
                val[3,3,:].= 2.0 .*sqrt(2.0) .*(14.0 .* s2t .+3.0.*s4t) ./den.^3
            end
        end
    elseif curve.id == eSPHERE
        s  = 0.5 .*t
        ct = cos.(t) ; st = sin.(t)
        cs = cos.(s) ; ss = sin.(s)
        dsdt = 0.5
        val[1,1,:] = ct .* cs
        val[1,2,:] = ct .* ss
        val[1,3,:] = ss
        if ders >= 1
            val[2,1,:] = .-st .* cs .-ct .*ss .* dsdt
            val[2,2,:] = .-st .* ss .+ct .*cs .* dsdt
            val[2,3,:] = cs .* dsdt
            if ders >= 2
                val[3,1,:].= .-ct .* cs .+st .*ss .*dsdt .+st .*ss .*dsdt .-ct .*cs .*dsdt^2
                val[3,3,:].= .-ct .* ss .-st .*cs .*dsdt .-st .*cs .*dsdt .-ct .*ss .*dsdt^2
                val[3,2,:].= -ss .* dsdt^2
            end
        end
    else
        @error " Wrong Curve ID!! "
    end
    return val
end


"""
computes the error curve e = | x - a o s|
Params:
-------
meshX, meshS:
    physical and parametric meshes
curve:
    the associated curve (a)
zw (default nothing):
    evaluation nodes (per element)
ele (default all):
    if > 1 returns only the error at that element
Returns:
--------
    Triad: {e, eT, eN}
    e total error
    eT tangent error
    eN normal error
"""
function error_decomposition(meshX::meshInfo, meshS::meshInfo, curve::curveInfo;
	                         z= nothing::Union{Nothing,gaussZW}, ele = 0::int)
    if z == nothing
        zw = gaussZW(eGLL, max(meshX.p, meshS.p) * 4)
        z  = zw.nodes
    end
    x    = evaluate_object(meshX,z,order=0,id=e)
    s    = evaluate_object(meshS,z,order=2,id=ele)
    a    = evaluate_object(curve,s[1,:],order=2,id=ele)
	tan  = a[2,:,:] .* s[2,1,:]'

	Q    = length(s[1,1,:])
    ntan = [norm(tan[:,j]) for j in 1:Q]
	T    = [tan[:,j] ./ ntan[j] for j in 1:Q]
	if curve.d == 2
		N = [-T[2,:] T[1,:]]'
	else
		N = zeros(3, Q)
		B = zeros(3, Q)
		for q = 1:Q
			d2a     = a[3,:,q] .* s[2,1,q]^2 .+a[2,:,q] .* s[3,1,q]
			dntan   = dot(d2a, tan[:,q]) / ntan[q]^3
			N[:,q]  = d2a ./ ntan[q] .- tan[:,q] .* dntan
			nN      = norm(N[:,q])
			N[:,q]./= nN
			B[:,q]  = cross(T[:,q],N[:,q])
		end
	end
	eEuc = colwise(Euclidean(),x,a[1,:,:])
	eTNB = zeros(curve.d,Q)
	xa   = x .- a[1,:,:]
    @einsum eTNB[1,g] = T[d,g] * xa[d,g]
    @einsum eTNB[2,g] = N[d,g] * xa[d,g]
	(curve.d == 3) && (@einsum eTNB[3,g] = B[d,g] * xa[d,g])
	return hcat(eEuc, eTNB')
end


"""
computes the arclength
Params:
-------
T:
    curve: analytic or egads object
t0,t1:
    interval
Returns:
    the arclength(T(t0,t1))
"""
function get_arc_length(T::curveInfo, t0::dbl, t1::dbl)
    T.ego != nothing && return egads.arcLength(T.ego, t0, t1)
    n   = 50
    if t1 < t0
        aux = t1
        t1  = t0
        t0  = aux
    end
    h   = (t1 - t0) / n
    zw  = gaussZW(eGLL, 10)
    a   = t0
    arc = 0.0
    for e = 1:n
        b    = a + h
        xyz  = evaluate_object(T, 0.5 * (h .* zw.nodes .+ (a + b)), order=1)
        arc += sum([norm(xyz[2,:,j]) for j in 1:length(zw.nodes)] .* zw.weights) * 0.5 * h
        a    = b
    end
    return arc
end


function check_arc_length(C::curveInfo, s::meshInfo)
	k0 = 1
	dist = 0.0
	refE = 0.0

	for k = 1 : C.nE
		s0 = s.nodes[1,k0]
		k0 += s.p
		s1 = s.nodes[1,k0]
		arc = egads.arcLength(C.ego,s0,s1)
		if k == 1
			refE = arc
		else
			dist = max(dist, abs(arc-refE))
		end
	end
	return (dist < 1.e-12 , dist)
end

function set_mesh_nodes_ids(d::Int, p::Int, n::Int;
	                        fix = 1::Int, loop = Int[]::vInt)
    totD = [1:1:n * p + 1;]
    totC = [1:1:d * (n * p + 1);]
    if !isempty(loop)
        totD[loop[1]] = totD[loop[2]]
        for k in 1:d
            totC[d * (loop[1] - 1) + k]  = totC[d * (loop[2] - 1) + k]
        end
    end

	pivD = fix == 0 ? [totD[1], totD[end]] : fix == 1 ? totD[1:p:totD[end]] : Int[]
	pivC = fix == 0 ? vcat(totC[1:d], totC[totC[end] - d + 1:totC[end]]) : fix == 1 ?
                      vcat([totC[d *p*j + 1:d * (p * j + 1) ] for j =0:n]...) : Int[]

    freeD = deleteat!(unique(totD), unique(pivD))
    freeC = deleteat!(unique(totC), unique(pivC))
    idsD  = zeros(Int,n, p + 1)
    idsC  = zeros(Int,n, d * (p + 1))
    for i in 1:n
        idsD[i,:] = totD[  (i-1)*p+1:   i*p+1]
        idsC[i,:] = totC[d*(i-1)*p+1:d*(i*p+1)]
    end
    return idsD, idsC, freeD, freeC
end

#=
function fixed_nodes(T::meshInfo)
    fD = sort(setdiff([1:1:      length(T.nodes[1,:]);],T.freeNodesList))
    fC = sort(setdiff([1:1:T.d * length(T.nodes[1,:]);],T.freeColNodesList))
    return fD,fC
end

function fixed_nodes_XS(meshX::meshInfo, meshS::meshInfo)
    xFix, xCFix = fixed_nodes(meshX)
    sFix, sCFix = fixed_nodes(meshS)
    return vcat(xCFix, length(meshX.nodes[1,:]) * meshX.d .+ sCFix)
end
=#
function free_nodes_XS(meshX::meshInfo, meshS::meshInfo)
    return vcat(meshX.freeColNodesList,
        length(meshX.nodes[1,:]) * meshX.d .+ meshS.freeColNodesList)
end

function collapse_XS_vars(meshX::meshInfo, meshS::meshInfo)
    nX = meshX.d * length(meshX.nodes[1,:])
    nS = meshS.d * length(meshS.nodes[1,:])
    xs = zeros(nX + nS)
    for j in 1:meshX.d
        xs[j:meshX.d:nX] = meshX.nodes[j,:]
    end
    for j in 1:meshS.d
        xs[j + nX:meshS.d:nX + nS] = meshS.nodes[j,:]
    end
    return xs
end

function p_refine_mesh(curve, pX, pS, inMX, inMS,fixX, fixS,eQT = eGLL)
    nE = inMX.nE
    nodS = zeros(inMS.d,nE * pS + 1)
    nodX = zeros(inMS.d,nE * pX + 1)
    piv  = [1:inMS.p:length(inMS.nodes[1,:]);]
    zS   = gaussZW(eQT, pS+1)
    zX   = gaussZW(eQT, pX+1)
    for e = 1:nE
        js = (e - 1)*pS+ 1
        jx = (e - 1)*pX+ 1
        a                  = inMS.nodes[1,piv[e] ]
        b                  = inMS.nodes[1,piv[e+1]]
        nodS[1,js:1:js+pS] = 0.5 .* ( (b - a) .* zS.nodes .+ a .+ b)
        nodX[1,jx:1:jx+pX] = 0.5 .* ( (b - a) .* zX.nodes .+ a .+ b)
    end
    loopS = Int[]
    sID, sCID, freeS, freeSC = set_mesh_nodes_ids(1, pS, nE, fix=fixS, loop=loopS)
	meshS = meshInfo(nodS, pS, nE, 1,zeros(3,100, pS + 1),freeS, freeSC, sID, sCID, loopS,)

    nodeX = evaluate_object(curve,nodX[1,:])

    loopX = curve.closed ? [length(nodX[1,:]),1] : Int[]
    xID, xCID, freeX, freeXC = set_mesh_nodes_ids(curve.d, pX, nE, fix=fixX, loop=loopX)
    meshX = meshInfo(nodeX, pX, nE, curve.d, zeros(3,100, pX + 1),freeX, freeXC, xID, xCID, loopX)
    return meshX, meshS
end

function h_refine_mesh(curve, pX, pS, meshS, fixX, fixS, type = eEQUIPARAM)
    piv   = [max(1,j * pS+1) for j = 0:meshS.nE]
    part0 = meshS.nodes[1,piv]
    nE    = 2 * (length(part0) - 1)
    part1 = zeros(nE + 1)
    part1[1:2:end] = part0
    nodeS  = zeros(1,nE*pS+1)
    eQT    = eGLL
    zwS    = gaussZW(eQT  , pS + 1)
    zwX    = gaussZW(eQT  , pX + 1)
    auX    = zeros(1,nE*pX+1)
    if type == eEQUIPARAM
        part1[2:2:end-1] = [0.5*(part0[j]+part0[j+1]) for j = 1:length(part0)-1]
    else
        for j = 1:length(part0)-1
            arc = get_arc_length(curve,part0[j], part0[j+1]) * 0.5
            part1[2 * (j-1) + 2] = inv_arc_length(curve, egads.arc, part0[j], part0[j+1])
            arc0 = get_arc_length(curve,part1[2*(j-1)+1], part1[2*(j-1)+2])
            arc1 = get_arc_length(curve,part1[2*(j-1)+2], part1[2*(j-1)+3])
        end
    end
    if type == eEQUIPARAM
        for j = 1:nE
            js = (j - 1) * pS + 1
            jx = (j - 1) * pX + 1
            nodeS[1,js:1:js+pS] = 0.5 .*( (part1[j+1] .- part1[j]) .* zwS.nodes .+ part1[j] .+ part1[j+1])
            auX[1,jx:1:jx+pX]   = 0.5 .*( (part1[j+1] .- part1[j]) .* zwX.nodes .+ part1[j] .+ part1[j+1])
        end
    else
        for j = 1:nE
            arc = get_arc_length(curve,part1[j], part1[j+1])
            js    = (j - 1) * pS + 1
            nodeS[1,js + pS] = part1[j]
            prop  = egads.arc / (zwS.nodes[end] - zwS.nodes[1])
            for k = 1:pS - 1
                nodeS[1,js + k] = inv_arc_length(curve, prop * (zwS.nodes[k+1] - zwS.nodes[k]), nodeS[1,js + k - 1], part1[j+1])
            end
            nodeS[1,js + pS] = part1[j+1]

            jx    = (j - 1) * pX + 1
            auX[1,jx + pX] = part1[j]
            prop  = egads.arc / (zwX.nodes[end] - zwX.nodes[1])

            for k = 1:pX - 1
                auX[1,jx + k] = inv_arc_length(curve, prop * (zwX.nodes[k+1] - zwX.nodes[k]), auX[1,jx + k - 1], part1[j+1])
            end

            auX[1,jx + pX] = part1[j+1]
        end
    end
    ## S MESH
    loopS = Int[]
    sID, sCID, freeS, freeSC = set_mesh_nodes_ids(1, pS, nE, fix=fixS, loop=loopS)
    meshS = meshInfo(nodeS, pS, nE, 1,zeros(3,100, pS + 1),freeS, freeSC, sID, sCID, loopS)

    nodeX = evaluate_object(curve, auX[1,:])
    loopX = curve.closed ? [length(nodeX[1,:]),1] : Int[]
    xID, xCID, freeX, freeXC = set_mesh_nodes_ids(curve.d, pX, nE, fix=fixX, loop=loopX)
    meshX = meshInfo(nodeX, pX, nE, curve.d, zeros(3,100, pX + 1),freeX, freeXC, xID, xCID, loopX)
    return meshX, meshS
end

function get_3d_nodes(mesh::meshInfo, curve::curveInfo)
	if curve.d == 3
		return mesh.nodes
	else
		dim = size(mesh.nodes)
		xx  = zeros(3, dim[2])
		ctt = filter(x -> !(x in curve.plane), [1,2,3])
		xx[curve.plane,:] .= mesh.nodes
		for j = 1:length(ctt)
			xx[ctt[j],:] .= curve.cttPlane[j]
		end
		return xx
	end
end

function sample_curves(meshX::meshInfo,meshS::meshInfo,curve::curveInfo;
	                   z = dbl[]::vDbl, ele = (-1)::int, make3d = false::Bool)

    isempty(z) && (z = gaussZW(eGLL, max(meshX.p, meshS.p) * 4).nodes)
    Q = length(z)
    x = evaluate_object(meshX,z,order=0,id=ele,all=false)
    s = evaluate_object(meshS,z,order=0,id=ele,all=false)
    a = evaluate_object(curve,s[1,:],order=0,id=ele,all=false)
	if make3d && curve.d != 3
		dim = size(x)
		xx  = zeros(3, dim[2]) ; aa = similar(xx)
		ctt = filter(x -> !(x in curve.plane), [1,2,3])
		xx[curve.plane,:] .= x
		aa[curve.plane,:] .= a
		for j = 1:length(ctt)
			xx[ctt[j],:] .= curve.cttPlane[j]
			aa[ctt[j],:] .= curve.cttPlane[j]
		end
		return xx, aa
	else return x, a
	end
end

function create_p1_mesh(curve, nE, type, inX = nothing, inS = nothing)

    zw = gaussZW(eGLL,10)
    if inX == nothing  || inS == nothing
        mx, ms = create_mesh(curve,nE,1,1,-1,-1,eEQUIPARAM, eGLL)
    else
        mx, ms = p_refine_mesh(curve,1,1,inX,inS, -1,-1,eGLL)
    end

    nE  = mx.nE
    eL2 = zeros(nE)
	for e = 1:nE
		x  = evaluate_object(mx, zw.nodes,order=1,id=e,all=true)
		s  = evaluate_object(ms, zw.nodes,order=0,id=e,all=false)
		a  = evaluate_object(curve, s[1,:])
		xa = [norm(x[1,:,k].-a[:,k]) for k = 1:zw.Q]
		eL2[e] = sum([norm(x[2,:,k]) * norm(x[1,:,k].-a[:,k])^2 * zw.weights[k] for k = 1:zw.Q])
	end
    local eAve = sum(eL2) / nE
    local it = 0
    while (any(eL2 ./ eAve .> 1.1) && it < 10)
        piv = findall(x -> x / eAve > 1.1, eL2)
        sN  = zeros(length(piv))
        for l = 1:length(piv)
            s0 = ms.nodes[1,piv[l]   ]
            s1 = ms.nodes[1,piv[l]+ 1]
            if type == eEQUIPARAM
                sN[l] = 0.5*(s0 + s1)
            else
                arc   = get_arc_length(curve,s0,s1) * 0.5
                sN[l] = inv_arc_length(curve, arc, s0,s1)
            end
        end
        newS  = sort(vcat(ms.nodes[1,:],sN))
        nE1   = length(newS) -1
        nodeX = evaluate_object(curve, newS)
        nodeS = zeros(1, nE1+1)
        nodeS[1,:] = newS
        loopX = curve.closed ? [length(nodeX[1,:]),1] : Int[]
        sID, sCID, freeS, freeSC = set_mesh_nodes_ids(1, 1,       nE1,fix=-1, loop=ms.loop)
        xID, xCID, freeX, freeXC = set_mesh_nodes_ids(curve.d, 1, nE1,fix=-1, loop=loopX)
        ms   = meshInfo(nodeS, 1, nE1,       1, zeros(3,100,2),freeS, freeSC, sID, sCID, ms.loop)
        mx   = meshInfo(nodeX, 1, nE1, curve.d, zeros(3,100,2),freeX, freeXC, xID, xCID, loopX)
        curve.nE = mx.nE
		for e = 1:nE1
			x  = evaluate_object(mx, zw.nodes,order=1,id=e,all=true)
			s  = evaluate_object(ms, zw.nodes,order=0,id=e,all=false)
			a  = evaluate_object(curve, s[1,:])
			xa = [norm(x[1,:,k].-a[:,k]) for k = 1:zw.Q]
			eL2[e] = sum([norm(x[2,:,k]) * norm(x[1,:,k].-a[:,k])^2 * zw.weights[k] for k = 1:zw.Q])
		end
        it   += 1
	end
    return mx, ms
end

function create_mesh(curve::curveInfo, nE::int, pX::int, pS::int ;
	                  fixX = 1::Int, fixS = 1::Int, meshType = eEQUIPARAM::String)
	!isempty(curve.tess) && ( nE = length(curve.tess) -1 )

	curve.nE = nE
	nodeS = set_curve_param(curve, nE, pS, meshType)
	loopS = Int[]
    sID, sCID, freeS, freeSC = set_mesh_nodes_ids(1, pS, nE, fix=fixS, loop=loopS)
	meshS = meshInfo(nodeS, pS, nE, 1,zeros(3,1, pS + 1),freeS, freeSC, sID, sCID, loopS)

	z     = gaussZW(eGLL,pX+1).nodes
	sx    = evaluate_object(meshS, z)
	cx    = evaluate_object(curve,sx[1,:])
	nodeX = zeros(curve.d, nE * pX+1)
	for j = 0:nE-1
		ox = j *  pX    + 1
		oc = j * (pX+1) + 1
		nodeX[:,ox:ox+pX] = cx[:,oc:oc+pX]
	end
	nodeX[:,end] = cx[:,end]
	loopX = curve.closed ? [nE * pX+1,1] : Int[]
    xID, xCID, freeX, freeXC = set_mesh_nodes_ids(curve.d, pX, nE, fix=fixX, loop=loopX)
    meshX = meshInfo(nodeX, pX, nE, curve.d, zeros(3,1, pX + 1),freeX, freeXC, xID, xCID, loopX)
	return meshX, meshS
end


function create_partitioned_mesh(C::curveInfo, pX::Int, pS::Int, nE::Int)
	pivX  = [1:pX+1;]; pivS = [1:pS+1;];
	xID, xCID, freeX, freeXC = set_mesh_nodes_ids(C.d, pX,1,fix=0)
	sID, sCID, freeS, freeSC = set_mesh_nodes_ids(1,   pS,1,fix=0)
	xI,sI = create_mesh(C, nE, pX,pS;meshType=eARCLENGTH);
	mXE   = [meshInfo(xI.nodes[:,i * pX .+ pivX], pX, 1, C.d, xI.phi,freeX, freeXC, xID, xCID,Int[]) for i = 0:nE-1]
	mSE   = [meshInfo(sI.nodes[:,i * pS .+ pivS], pS, 1, 1,   sI.phi,freeS, freeSC, sID, sCID,Int[]) for i = 0:nE-1]
	return mXE, mSE
end

refill_nodes!(curve::curveInfo,n1::mDbl,p::Int,n2::Array{Float64,3}) =
    [ n1[:,(e-1) * p .+ [1:p+1;]] = n2[e,size(n1,1) ==1 ? 1 : curve.plane,:] for e = 1:curve.nE ]


function set_curve_param(curve::curveInfo, nE::int, p::int, param::String)
    m            = nE * p
    nodeS        = zeros(1,m+1)
    zw           = gaussZW(eGLL, p + 1)
    if !isempty(curve.tess)
        for j = 1:length(curve.tess) -1
            off = (j - 1) * p + 1
            a   = curve.tess[j]; b = curve.tess[j+1]
            nodeS[1,off:1:off+p] = 0.5 .*( (b-a) .* zw.nodes .+ a .+ b)
        end
    elseif param == eARCLENGTH
		e_arc = get_arc_length(curve,curve.t0,curve.t1) /  Float64(nE)
		t0 = curve.t0
		nodeS[1,1] = t0
        for j = 1:nE
            jj   = (j - 1) * p + 1
            t1   = inv_arc_length(curve, e_arc, t0, curve.t1)
            #test = get_arc_length(curve,t0, t1)
            prop = e_arc / (zw.nodes[end] - zw.nodes[1])
			#nodeS[1,jj] = t0
			nodeS[1,jj:jj+p] = 0.5 .*( (t1-t0) .* zw.nodes .+ t1 .+ t0)
			#=for k = 1:p-1
				aux = inv_arc_length(curve, prop * (zw.nodes[k+1] - zw.nodes[k]), nodeS[1,jj + k-1], t1)
			    nodeS[1,jj + k] = aux#inv_arc_length(curve, prop * (zw.nodes[k+1] - zw.nodes[k]), nodeS[1,jj + k - 1], t1, true)
                test = get_arc_length(curve,nodeS[1,jj + k-1],nodeS[1,jj + k])
            end=#
			#nodeS[1,jj + p] = t1
            t0 = t1
        end
    else
        xi = vcat([1.0 .+ 2 * (j - 1) .+ zw.nodes  for j = 1:nE]...)
        t  = (curve.t1 - curve.t0) / xi[end] .* xi .+ curve.t0
        piv        = [p+1:p+1:length(t) - 1;]
        nodeS[1,:] = deleteat!(t, piv)
    end
    nodeS[1,  1] = curve.t0
    nodeS[1,end] = curve.t1
    return nodeS
end


"""
Starting from t0 (t1 if oriented backwards), finds the value t such that arclength(curve,t0,t) = arc

Params:
-------
curve:
	information of the underlying curve: analytic or CAD
arc:
	desired arc length
t0,t1:
	interval where to search
checkNewton (optional):
	if true, returns information on Newton convergence

Returns:
t:
	the parametric curve position
"""
function inv_arc_length(curve::curveInfo, arc::dbl, t0::dbl, t1::dbl, checkNewton = false::Bool)

	if checkNewton
        x0 = 0.0; x1 = 0.0; x2 = 0.0; e1 = -1.0; e2 = -1.0
    end

    if t0 > t1
		t0,t1 = t1,t0
	end

    nEPS = 1.e-13;

    tIT  = 0.5 * (t0 + t1)
    df   = 0.0 ; f = 0.0 ;  converged = false
    delta  = 0.0 ;

    t_step = tIT
    for it = 1:(nT=50)
        s = 1.0
        for ls = 1:(nS=20)
            tIT = t_step - delta * s
			(tIT > t0 && tIT < t1) && break
			s *= 0.5
        end
        t_step    = tIT
        f         = get_arc_length(curve, t0,tIT) - arc
        xyz  = evaluate_object(curve, t_step, order=1)
        df   = norm(xyz[2,:,1])
		converged = abs(f) < nEPS || abs(df) < nEPS
        converged && break
        delta = f / df
        if checkNewton
            x2 = abs(f / df)
            if (it == 0)
                x0 = x2
            elseif (it == 1)
                x1 = x2
            else
              e1 = abs(x1 / x0)
              e2 = abs(x2 / x1)
              x0 = x1
              x1 = x2
            end
        end
        converged = abs(delta) < nEPS
        converged && break
    end
    if checkNewton
        if (e1 >= 0.0 && e2 >= 0.0)
            @printf("Newton iterations %d convergence %lf (e1 %lf e2 %lf)\n",it, log(e2) / log(e1), e1, e2)
        else
            @printf("Newton iterations %d convergence NA\n", it)
        end
        converged = false
    end
    if !converged && checkNewton
        print("\n ----------> ARC LENGTH RESIDUAL ", f, "\n")
        if abs(df) < nEPS
            @printf(" derivative newton is %2.2e ~ 0\n", df)
        end
        if t_step > t1 || t_step < t0
            @printf(" GONE BEYOND LIMITS %lf != [%lf,%lf] !!! RETURNING MID POINT t = (t1+t0)/2\n", t_step,t0,t1)
            t_step = 0.5*(t1 + t0)
        end
    end
    return t_step
end


"""
Splits the curve from (t1,t2) such that the arc (t1,t) is a w-portion of the total arc (t1,t2)

Params:
-------
curve:
	information of the underlying curve: analytic or CAD
w:
	segment split: eg, 1/2 splits the interval in half (in arclength terms)

t1,t2:
	interval where to search

checkNewton (optional):
	if true, returns information on Newton convergence

Returns:
t3:
	the parametric curve position corresponding to the w-split
"""
function split_curve(curve::curveInfo, w::dbl, t1::dbl, t2::dbl, checkNewton =false::Bool)
    k    = w^2 / (1.0 - w)^2
    t3   = t2 * w + t1 * (1.0 - w)
    xyz = evaluate_object(curve,[t1 t2 t3],order=1)

    l    = [norm(xyz[1,:,3] .- xyz[1,:,1]), norm(xyz[1,:,3] .- xyz[1,:,2])]
    lt   = sum(l)
    if checkNewton
        @printf("\n Initial l0 = %lf l1 = %lf split edge in fracs %lf %lf (expected %lf %lf)\n",
             l[1], l[2], l[1] / lt, l[2] / lt, w, 1.0 - w)
        x0 = 0.0; x1 = 0.0; x2 = 0.0; e1 = -1.0; e2 = -1.0
    end
    dt   = 0.0
    nEPS = 1.e-10;
    it = 1; res = 0.0
    for it = 1:(nT=100)
        s = 1.0; f = 0.0; t = t3; i = 0;
        for i = 1:(nS=20)
            t  = t3 + s * dt
            pt = evaluate_object(curve,t, order=1)
            if pt == nothing
                s *= 0.5
                continue
            end
            if t < t1 || t > t2
                s *= 0.5
                continue
            end
            xyz[:,:,3] = pt
            l  = [norm(xyz[1,:,3] .- xyz[1,:,1]), norm(xyz[1,:,3] .- xyz[1,:,2])]
            f  = 0.5 * (l[1]^2 - k * l[2]^2)
            if (it == 1 || abs(f) < res)
                res = abs(f)
                break
            else
                s *= 0.5
            end
        end
        t3 = t;
        if (i == nS) @printf(" splitCurve: LineSearch Failed!\n")
        end
        if (i == nS || res < nEPS) break
        end
        ff = dot(xyz[2,:,3], xyz[1,:,3] .- xyz[1,:,1]) - k *
             dot(xyz[2,:,3], xyz[1,:,3] .- xyz[1,:,2])
        dt = - (f / ff)
        if checkNewton
            x2 = abs(dt)
            if     (it == 0)
                x0 = x2
            elseif (it == 1)
                x1 = x2
            else
              e1 = abs(x1 / x0)
              e2 = abs(x2 / x1)
              x0 = x1
              x1 = x2
            end
        end
        if (abs(dt) < nEPS) break
        end
    end
    if checkNewton
        if (e1 >= 0.0 && e2 >= 0.0)
            @printf("Newton iterations %d convergence %lf (e1 %lf e2 %lf)\n",it, log(e2) / log(e1), e1, e2)
        else
            @printf("Newton iterations %d convergence NA\n", it)
        end
        l  = [norm(xyz[1,:,3] .- xyz[1,:,1]), norm(xyz[1,:,3] .- xyz[1,:,2])]
        lt = sum(l)
        @printf("\n FINAL l0 = %lf l1 = %lf split edge in fracs %lf %lf (expected %lf %lf)\n",
                 l[1], l[2], l[1] / lt, l[2] / lt, w, 1.0 - w)
    end
    if (res >= nEPS && abs(dt) >= nEPS)
       @printf(" splitCurve din't converge -- residual %1.2le delta %1.2le (%1.2le)!\n",res, abs(dt), nEPS)
    end
    if (t3 < t1 || t3 > t2 )
       @printf(" splitCurve solution out-of-range!\n")
    end
    return t3
end


function auto_curve(type::int)
    if type == eCIRCLE
        return curveInfo(2,0.0, 2.0; a =1.0, b=pi, id=type, closed=true)
    elseif type == eSEMICIRCLE
        return curveInfo(2,0.0, 1.0; a =1.0, b=pi, id=type)
    elseif type == eCIRCLEXY
        return curveInfo(2,-1.0, 1.0; a =1.0, b=pi, id=type)
    elseif type == eCIRCLEPOL
        return curveInfo(2,0.01^(1/3), 1.0; a =1.0, b = 2.0, id = type)
    elseif type == eCIRCLE3D
        return curveInfo(3,0.0, pi ;  a = 1.0, b = 1.0, id = type)
    elseif type == ePOLY3D
        return curveInfo(3,0.0,1.0, a = 1.0, b = 1.0, id = type)
    elseif type == eSPIRAL
        return curveInfo(2,0.0, 12.5; a=1.0, b=0.1 ,id = type)
    elseif type == eLEMNISCATA
        return curveInfo(2,0.0, 2.0*pi, a=1.5, b=0.1, id = type)
    elseif type == eARCSPIRAL
        aux = curveInfo(2,0.0, 12.5, a=1.0, b=0.1, id = type)
        return curveInfo(2,0.0, get_arc_length(aux,aux.t0,aux.t1), a = 1.0, b=0.1, id = type)
    elseif type == eSPIRAL3D
        return curveInfo(3,2*pi, 6 * pi, a=1.0, b=0.0,  id = type)
    elseif type == eSPHERE
        return curveInfo(3,0, 0.5 * pi, a = 1.0, b = 0.0, id = type)
    elseif type == ePETALS
        return curveInfo(2,0.0, pi, a = 3.0, b = 0.25, id = type)
    elseif type == eCUBIC
        return curveInfo(2,0.0, 1.0; a=1.0, b=0.0, id = type)
    elseif type == eHELIX
        return curveInfo(3,0.0, 1.0; a=1.0, b=2.0, id = type)
    elseif type == eARCHYTAS
        return curveInfo(3,0.0, 2.0 .* pi, a=1.0, b=2.0,id = type, closed = true)
    elseif type == eNACA
        return curveInfo(2,-1.0, 1.0; a = 1.0, b = 0.12,id = type, closed = true)
    elseif type == ePOLY2D
        return curveInfo(2,-1.0,1.0; a = 1.0,b=1.0,id = type)
    else
        @error " CURVE NOT RECOGNISED. CHECK TYPE "
        return nothing
    end
end
